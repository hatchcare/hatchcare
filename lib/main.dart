import 'dart:ffi' hide Size;
import 'dart:io';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/screens/correlation/correlation_page.dart';
import 'package:hatchcare/screens/graphs/graph_page.dart';
import 'package:hatchcare/screens/question/question_page.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rive/rive.dart';

late final NativeLibrary lib;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  if (!kIsWeb && (Platform.isLinux || Platform.isWindows || Platform.isMacOS)) {
    // Set window to a smartphone size
    await DesktopWindow.setWindowSize(const Size(1440 / 4, 2960 / 4));
  }

  DynamicLibrary nativeApiLib = Platform.isMacOS || Platform.isIOS
      ? DynamicLibrary.process() // macOS and iOS
      : (DynamicLibrary.open(Platform.isWindows // Windows
          ? 'hatchcareBack.dll'
          : 'libhatchcareBack.so')); // Android and Linux
  lib = NativeLibrary(nativeApiLib);

  runApp(const MyApp());
}

/// The main application widget.
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (BuildContext context, Orientation orientation,
          ScreenType screenType) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            colorScheme: ColorScheme(
              brightness: Brightness.light,
              primary: Colors.white,
              onPrimary: Colors.grey[800]!,
              secondary: const Color(0xFF70DDFF),
              onSecondary: const Color.fromARGB(255, 255, 255, 255),
              error: const Color.fromARGB(255, 255, 0, 0),
              onError: const Color.fromARGB(255, 255, 255, 255),
              background: const Color(0xFFAEEBFF),
              onBackground: const Color.fromARGB(255, 255, 255, 255),
              surface: const Color.fromARGB(255, 39, 167, 187),
              onSurface: Colors.grey[800]!,
            ),
          ),
          home: const HomePage(),
        );
      },
    ); // Sizer
  }
}

/// The home page widget.
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  StateMachineController? _controller;

  void _onRiveInit(Artboard artboard) {
    _controller = StateMachineController.fromArtboard(artboard, 'hatchip');
    artboard.addController(_controller!);
    _controller?.isActive = true;
    Future.delayed(const Duration(milliseconds: 500),
        () => (_controller?.findInput<bool>("hatched")?.value = true));
  }

  @override
  Widget build(BuildContext context) {
    _controller?.isActive = true;
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'HatchCare',
            style: TextStyle(
              fontSize: 20.sp,
              color: Colors.grey[800],
            ),
          ),
        ),
        backgroundColor: Theme.of(context).colorScheme.secondary,
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 100.w,
              height: 100.w,
              child: RiveAnimation.asset(
                'assets/hatchip.riv',
                fit: BoxFit.fitWidth,
                onInit: _onRiveInit,
              ),
            ),
            SizedBox(height: 5.h),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: AnimatedTextKit(
                animatedTexts: [
                  for (var i in Res.welcomMessages)
                    TypewriterAnimatedText(i,
                        curve: Curves.easeInOut,
                        textStyle: TextStyle(
                          color: Colors.grey[800],
                        )),
                ],
                isRepeatingAnimation: true,
                repeatForever: true,
              ),
            ),
            const Spacer(),
            Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                Container(
                  color: Theme.of(context).colorScheme.secondary,
                  width: 100.w,
                  height: 10.h,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const GraphPage()));
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              Theme.of(context).colorScheme.primary,
                          elevation: 5,
                          fixedSize: Size(15.w, 15.w),
                          padding: const EdgeInsets.all(0),
                        ),
                        child: Icon(
                          Icons.analytics,
                          size: 30.sp,
                          color: Colors.grey[800],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5.h),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const QuestionPage()));
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                Theme.of(context).colorScheme.primary,
                            elevation: 5,
                            fixedSize: Size(20.w, 20.w),
                            padding: const EdgeInsets.all(0),
                          ),
                          child: Icon(
                            Icons.question_mark_rounded,
                            size: 30.sp,
                            color: Colors.grey[800],
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const CorrelationPage()));
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor:
                              Theme.of(context).colorScheme.primary,
                          elevation: 5,
                          fixedSize: Size(15.w, 15.w),
                          padding: const EdgeInsets.all(0),
                        ),
                        child: Icon(
                          Icons.stacked_line_chart_rounded,
                          size: 30.sp,
                          color: Colors.grey[800],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
