import 'package:path_provider/path_provider.dart';

/// A class that provides various resources and constants used in the application.
class Res {
  /// Returns the path to the database file.
  static Future<String> dbPath() async =>
      "${(await getApplicationSupportDirectory()).absolute.path}/db.csv";

  /// Returns the path to the temporary response file.
  static Future<String> tmpResponsePath() async =>
      "${(await getTemporaryDirectory()).absolute.path}/reponseTmp.csv";

  /// List of welcome messages displayed to the user.
  static const List<String> welcomMessages = [
    "Hey comment ça va aujourd'hui ?",
    "Réponds vite à un questionnaire pour avoir des statistiques sur ton bien-être !"
  ];

  /// The text displayed as a placeholder for user input.
  static const String enterText = "Entrez votre réponse ici";

  /// The text displayed when a question is not answered.
  static const String pleaseAnswer = "Veuillez répondre à la question";

  /// The text displayed when there is no data to display.
  static const String noDataToDisplay = "Aucune donnée à afficher";

  /// The text displayed when selecting a date.
  static const String selectDate = "Sélectionnez une date";

  /// The text displayed for the "Previous" button.
  static const String previous = "Précédent";

  /// The text displayed for the "Next" button.
  static const String next = "Suivant";

  /// The text displayed for the "Correlation" button.
  static const String correlation = "Corrélation";

  /// The threshold value for high correlation.
  static const double highCorrelation = 0.7;

  /// The threshold value for medium correlation.
  static const double mediumCorrelation = 0.5;

  /// The threshold value for low correlation.
  static const double lowCorrelation = 0.3;

  /// The threshold value for very low correlation.
  static const double veryLowCorrelation = 0.1;

  /// A map that maps category names to their French translations.
  static const Map<String, String> categoryToFrench = {
    "noteMood": "note d'humeur",
    "adjective": "adjectif",
    "rant": "coup de gueule",
    "sleepBegin": "début de sommeil",
    "sleepEnd": "fin de sommeil",
    "sleepQuality": "qualité de sommeil",
    "sleepPlus": "plus de sommeil",
    "mealHour": "heure de repas",
    "mealQuality": "qualité de repas",
    "mealPlus": "plus de repas",
  };

  /// Generates a sentence describing the correlation between two categories.
  static String correlationSentence(
      String categoryA, String categoryB, double correlation, int lag) {
    String strenghAdjective = correlationStrength(correlation);
    return "Il y a une corrélation $strenghAdjective entre $categoryA et $categoryB avec un coefficient de $correlation et un décalage de $lag jour(s).";
  }

  /// Determines the strength of a correlation based on its value.
  static String correlationStrength(double correlation) {
    correlation = correlation.abs();
    if (correlation < veryLowCorrelation) {
      return "très faible";
    } else if (correlation < lowCorrelation) {
      return "faible";
    } else if (correlation < mediumCorrelation) {
      return "moyenne";
    } else if (correlation < highCorrelation) {
      return "forte";
    } else {
      return "très forte";
    }
  }
}

/// Enum representing the fields in the data.
enum Fields {
  date,
  noteMood,
  adjective,
  rant,
  sleepBegin,
  sleepEnd,
  sleepQuality,
  sleepPlus,
  mealHour,
  mealQuality,
  mealPlus,
}
