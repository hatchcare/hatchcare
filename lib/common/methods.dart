import 'dart:ffi';

import 'package:ffi/ffi.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';

/// Fetches data for the graph page.
///
/// This method fetches data for each enabled field and returns a list of tuples
/// containing the field name and its corresponding chart data.
/// The chart data is a list of tuples, where each tuple contains two integers (a, b).
/// The integers represent the date and the data value respectively.
///
/// Returns a Future that resolves to a list of tuples, where each tuple contains
/// a field name and its corresponding chart data.
Future<List<(String name, List<(DateTime a, int b)>)>> fetchData(
    responseList_t responses, List<bool> enabled) async {
  List<(String name, List<(DateTime a, int b)>)> result = [];
  if (!enabled.any((element) => element)) {
    return result;
  }
  for (var i in Fields.values) {
    if (enabled[i.index]) {
      var data = lib.responsesGetIntArray(
          responses, i.name.toNativeUtf8() as Pointer<Char>);
      var date = lib.responsesGetDate(responses);
      List<(DateTime a, int b)> chartData = [];
      for (int i = 0; i < data.size; i++) {
        chartData.add((
          DateTime.fromMillisecondsSinceEpoch(date.data[i] * 1000),
          data.data[i]
        ));
      }
      result.add((i.name, chartData));
      lib.intListFree(data);
      lib.longListFree(date);
    }
  }

  return result;
}
