import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:hatchcare/common/methods.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

/// A widget that displays the correlation between two categories.
class CorrelationWidget extends StatefulWidget {
  const CorrelationWidget({
    super.key,
    required this.correlationData,
    required this.responseList,
  });

  final correlation_t correlationData;
  final responseList_t responseList;

  @override
  State<CorrelationWidget> createState() => _CorrelationWidgetState();
}

class _CorrelationWidgetState extends State<CorrelationWidget> {
  @override
  Widget build(BuildContext context) {
    String categoryA = Res.categoryToFrench[lib
        .correlationGetQuestionTypeA(widget.correlationData)
        .cast<Utf8>()
        .toDartString()]!;
    String categoryB = Res.categoryToFrench[lib
        .correlationGetQuestionTypeB(widget.correlationData)
        .cast<Utf8>()
        .toDartString()]!;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: AnimatedTextKit(
            animatedTexts: [
              TypewriterAnimatedText(
                Res.correlationSentence(
                  categoryA,
                  categoryB,
                  lib.correlationGetCoefficient(widget.correlationData),
                  lib.correlationGetLag(widget.correlationData),
                ),
                curve: Curves.easeInOut,
                textStyle: TextStyle(
                  color: Colors.grey[800],
                ),
              ),
            ],
            isRepeatingAnimation: false,
          ),
        ),
        FutureBuilder(
          future: fetch(),
          builder: (context, snap) {
            if (snap.hasData) {
              return SfCartesianChart(
                series: [
                  for (var i in snap.data!)
                    if (i.$2.isNotEmpty)
                      SplineSeries<(DateTime a, int b), DateTime>(
                        dataSource: i.$2,
                        xValueMapper: (data, _) => data.$1,
                        yValueMapper: (data, _) => data.$2,
                        legendItemText: i.$1,
                        legendIconType: LegendIconType.circle,
                        yAxisName: snap.data!.indexOf(i).toString(),
                      )
                ],
                axes: [
                  for (int i = 0; i < snap.data!.length; i++)
                    NumericAxis(
                      name: i.toString(),
                      isVisible: false,
                    )
                ],
                legend: const Legend(
                  isVisible: true,
                  position: LegendPosition.bottom,
                ),
                primaryYAxis: const NumericAxis(
                  isVisible: false,
                ),
                primaryXAxis: DateTimeAxis(
                  dateFormat: DateFormat.yMd(),
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          },
        ),
      ],
    );
  }

  /// Fetches the data for the correlation chart.
  Future<List<(String, List<(DateTime, int)>)>> fetch() async {
    List<bool> values = [];
    List<String> stringValues = [
      lib
          .correlationGetQuestionTypeA(widget.correlationData)
          .cast<Utf8>()
          .toDartString(),
      lib
          .correlationGetQuestionTypeB(widget.correlationData)
          .cast<Utf8>()
          .toDartString()
    ];
    for (int i = 0; i < Fields.values.length; i++) {
      values.add(stringValues.contains(Fields.values[i].name));
    }
    List<(String, List<(DateTime, int)>)> data =
        await fetchData(widget.responseList, values);

    // Apply the lag
    int lag = lib.correlationGetLag(widget.correlationData);
    if (lag != 0) {
      if (lag > 0) {
        data[0] = (data[0].$1, data[0].$2.sublist(0, data[0].$2.length - lag));
        data[1] = (data[1].$1, data[1].$2.sublist(lag));
      } else {
        data[0] = (data[0].$1, data[0].$2.sublist(-lag));
        data[1] = (data[1].$1, data[1].$2.sublist(0, data[1].$2.length + lag));
      }
    }
    return data;
  }
}
