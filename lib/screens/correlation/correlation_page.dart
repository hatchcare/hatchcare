// ignore_for_file: use_build_context_synchronously

import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';
import 'package:hatchcare/screens/correlation/correlation_widget.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rive/rive.dart';

/// The page that displays correlations between responses.
class CorrelationPage extends StatefulWidget {
  const CorrelationPage({super.key});

  @override
  State<CorrelationPage> createState() => _CorrelationPageState();
}

class _CorrelationPageState extends State<CorrelationPage> {
  responseListStruct? responseList;
  correlationList? correlations;
  bool initialized = false;

  PageController pageController = PageController();

  StateMachineController? _controller;

  @override
  void initState() {
    initAsync();
    super.initState();
  }

  /// Initializes the page asynchronously.
  void initAsync() async {
    if (!File(await Res.dbPath()).existsSync()) {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text(Res.noDataToDisplay)));
      return;
    }
    responseList = lib.responsesFromFile(
        (await Res.dbPath()).toNativeUtf8() as Pointer<Char>);
    correlations = lib.responsesGetCorrelationArray(responseList!);
    setState(() {
      initialized = true;
    });
  }

  @override
  void dispose() {
    if (responseList != null) {
      lib.responsesFree(responseList!);
    }
    if (correlations != null) {
      lib.correlationListFree(correlations!);
    }
    super.dispose();
  }

  void _onRiveInit(Artboard artboard) {
    _controller = StateMachineController.fromArtboard(artboard, 'hatchip')!;
    artboard.addController(_controller!);
    _controller!.isActive = true;
    _controller!.findInput<bool>("hatched")?.value = true;
    (_controller!.findInput<bool>("to_happy") as SMITrigger).fire();
  }

  @override
  Widget build(BuildContext context) {
    if (initialized) {}
    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              Res.correlation,
              style: TextStyle(
                fontSize: 20.sp,
                color: Colors.grey[800],
              ),
            ),
          ),
          backgroundColor: Theme.of(context).colorScheme.secondary,
        ),
        body: (initialized)
            ? Column(
                children: [
                  SizedBox(
                    width: 50.w,
                    height: 50.w,
                    child: RiveAnimation.asset(
                      'assets/hatchip.riv',
                      fit: BoxFit.fitWidth,
                      onInit: _onRiveInit,
                    ),
                  ),
                  Flexible(
                    child: PageView(
                      controller: pageController,
                      scrollDirection: Axis.horizontal,
                      children: [
                        for (int i = 0; i < correlations!.size; i++)
                          if (lib
                                  .correlationGetCoefficient(
                                      correlations!.correlations[i])
                                  .abs() >=
                              Res.mediumCorrelation)
                            CorrelationWidget(
                              correlationData: correlations!.correlations[i],
                              responseList: responseList!,
                            )
                      ],
                    ),
                  ),
                ],
              )
            : const CircularProgressIndicator(),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.grey[800],
          unselectedItemColor: Colors.grey[800],
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.arrow_back),
              label: Res.previous,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.arrow_forward),
              label: Res.next,
            ),
          ],
          onTap: (index) {
            if (index == 0) {
              pageController.previousPage(
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeInOut,
              );
            } else {
              pageController.nextPage(
                duration: const Duration(milliseconds: 500),
                curve: Curves.easeInOut,
              );
            }
          },
        ));
  }
}
