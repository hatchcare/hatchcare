// ignore_for_file: use_build_context_synchronously

import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:hatchcare/common/methods.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/main.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../generated_bindings.dart';

/// A page that displays graphs based on user responses.
class GraphPage extends StatefulWidget {
  const GraphPage({super.key});

  @override
  State<GraphPage> createState() => _GraphPageState();
}

class _GraphPageState extends State<GraphPage> {
  List<bool> enabled = List.filled(Fields.values.length, false);
  responseList_t? responses;

  @override
  void initState() {
    enabled[1] = true;
    initStateAsync();
    super.initState();
  }

  void initStateAsync() async {
    if (!File(await Res.dbPath()).existsSync()) {
      Navigator.pop(context);
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text(Res.noDataToDisplay)));
      return;
    }
    responses = lib.responsesFromFile(
        (await Res.dbPath()).toNativeUtf8() as Pointer<Char>);
    setState(() {});
  }

  @override
  void dispose() {
    if (responses != null) {
      lib.responsesFree(responses!);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: (responses != null)
                  ? FutureBuilder(
                      future: fetchData(responses!, enabled),
                      builder: (context, snap) {
                        if (snap.hasData) {
                          return SfCartesianChart(
                            series: [
                              for (var i in snap.data!)
                                if (i.$2.isNotEmpty)
                                  SplineSeries<(DateTime a, int b), DateTime>(
                                    dataSource: i.$2,
                                    xValueMapper: (data, _) => data.$1,
                                    yValueMapper: (data, _) => data.$2,
                                    dataLabelSettings: const DataLabelSettings(
                                      isVisible: false,
                                    ),
                                    legendItemText: i.$1,
                                    legendIconType: LegendIconType.circle,
                                    yAxisName: snap.data!.indexOf(i).toString(),
                                  )
                            ],
                            legend: const Legend(
                              isVisible: true,
                              position: LegendPosition.bottom,
                            ),
                            primaryXAxis: const DateTimeAxis(),
                            primaryYAxis: const NumericAxis(
                              isVisible: false,
                            ),
                            axes: [
                              for (int i = 0; i < snap.data!.length; i++)
                                NumericAxis(
                                  name: i.toString(),
                                  isVisible: false,
                                )
                            ],
                          );
                        } else {
                          return const CircularProgressIndicator();
                        }
                      })
                  : const CircularProgressIndicator(),
            ),
            SizedBox(
              width: 100.w,
              height: 40.h,
              child: GridView(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 200,
                  childAspectRatio: 4.0,
                ),
                children: [
                  for (var i in Fields.values)
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Checkbox(
                          value: enabled[i.index],
                          onChanged: (value) {
                            setState(() {
                              enabled[i.index] = value!;
                            });
                          },
                        ),
                        Text(i.toString().split('.').last),
                      ],
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
