import 'package:flutter/material.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/common/res.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

/// A widget that displays a text input field for answering a question.
class QuestionText extends StatelessWidget {
  /// Constructs a [QuestionText] widget.
  ///
  /// The [questionData] parameter is the data for the question being displayed.
  /// The [getAnswer] parameter is a callback function that is called when the
  /// user enters a new answer.
  const QuestionText({
    super.key,
    required this.questionData,
    required this.getAnswer,
  });

  /// The data for the question being displayed.
  final question_t? questionData;

  /// A callback function that is called when the user enters a new answer.
  final void Function(String) getAnswer;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 80.w,
      child: TextField(
        decoration: const InputDecoration(
          labelText: Res.enterText,
        ),
        onChanged: (value) {
          getAnswer(value);
        },
        maxLines: 4,
      ),
    );
  }
}
