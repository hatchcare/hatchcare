import 'package:flutter/material.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

/// A widget that displays a slider for selecting a numeric value for a question.
class QuestionNumber extends StatefulWidget {
  /// Constructs a [QuestionNumber] widget.
  ///
  /// The [questionData] parameter is the data for the question.
  /// The [getAnswer] parameter is a callback function that is called when the value is changed.
  const QuestionNumber({
    super.key,
    required this.questionData,
    required this.getAnswer,
  });

  /// The data for the question.
  final question_t? questionData;

  /// A callback function that is called when the value is changed.
  final void Function(int) getAnswer;

  @override
  State<QuestionNumber> createState() => _QuestionNumberState();
}

class _QuestionNumberState extends State<QuestionNumber> {
  late final int min;
  late final int max;
  late final int step;
  late int value;

  @override
  void initState() {
    min = lib.questionNumberGetMin(widget.questionData!);
    max = lib.questionNumberGetMax(widget.questionData!);
    step = lib.questionNumberGetStep(widget.questionData!);
    value = ((max - min) / 2).round() +1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Slider(
            value: value.toDouble(),
            min: min.toDouble(),
            max: max.toDouble(),
            divisions: (max - min) ~/ step,
            onChanged: (double value) {
              setState(() {
                this.value = value.toInt();
                widget.getAnswer(value.toInt());
              });
            },
          ),
          Text(
            value.toString(),
            style: TextStyle(fontSize: 20.sp),
          ),
        ],
      ),
    );
  }
}
