import 'dart:ffi';

import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';

class QuestionAdjectif extends StatefulWidget {
  const QuestionAdjectif({
    super.key,
    required this.questionData,
    required this.getAnswer,
  });

  final question_t? questionData;
  final void Function(String) getAnswer;

  @override
  State<QuestionAdjectif> createState() => _QuestionAdjectifState();
}

class _QuestionAdjectifState extends State<QuestionAdjectif> {
  late List<String> adjectifs;

  late String? adjectifValue;
  late String? subAdjectifValue;

  @override
  void initState() {
    adjectifValue = null;
    subAdjectifValue = null;
    adjectifs = [];
    var adj = lib.adjectiveGet();
    for (int i = 0; i < adj.len; i++) {
      adjectifs.add((adj.array[i]).cast<Utf8>().toDartString());
    }
    lib.stringArrayFree(adj);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DropdownButton(
          items: adjectifs.map((String adjectif) {
            return DropdownMenuItem(
              value: adjectif,
              child: Text(adjectif),
            );
          }).toList(),
          value: adjectifValue,
          onChanged: (String? value) {
            setState(() {
              adjectifValue = value!;
            });
          },
        ),
        if (adjectifValue != null)
          DropdownButton(
            items: subAjd(adjectifValue!).map((String subAdjectif) {
              return DropdownMenuItem(
                value: subAdjectif,
                child: Text(subAdjectif),
              );
            }).toList(),
            value: subAdjectifValue,
            onChanged: (String? value) {
              setState(() {
                subAdjectifValue = value!;
                widget.getAnswer(subAdjectifValue!);
              });
            },
          ),
      ],
    );
  }

  List<String> subAjd(String ajd) {
    var subAjd = lib.adjectivesGetSub(ajd.toNativeUtf8() as Pointer<Char>);
    List<String> subAjdList = [];
    for (int i = 0; i < subAjd.len; i++) {
      subAjdList.add((subAjd.array[i]).cast<Utf8>().toDartString());
    }

    lib.stringArrayFree(subAjd);
    if (!subAjdList.contains(subAdjectifValue)) {
      subAdjectifValue = null;
    }
    return subAjdList;
  }
}
