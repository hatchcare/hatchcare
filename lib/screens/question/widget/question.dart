import 'dart:ffi' hide Size;
import 'dart:io';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';
import 'package:hatchcare/screens/question/widget/question_adjectif.dart';
import 'package:hatchcare/screens/question/widget/question_date.dart';
import 'package:hatchcare/screens/question/widget/question_number.dart';
import 'package:hatchcare/screens/question/widget/question_text.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

/// The Question widget displays a question and handles user responses.
class Question extends StatefulWidget {
  const Question({
    super.key,
    required this.questionData,
    required this.responseData,
    required this.callback,
  });

  final question_t questionData;
  final response_t responseData;
  final void Function(String) callback;

  @override
  State<Question> createState() => _QuestionState();
}

class _QuestionState extends State<Question> {
  late question_t questionData;

  @override
  void initState() {
    questionData = widget.questionData;
    super.initState();
  }

  @override
  void dispose() {
    lib.reponseFree(widget.responseData);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    dynamic response;
    Widget questionWidget;
    String type =
        lib.questionGetQuestionType(questionData).cast<Utf8>().toDartString();
    switch (type) {
      case "adjective":
        questionWidget = QuestionAdjectif(
          key: UniqueKey(),
          questionData: questionData,
          getAnswer: (String value) {
            response = value;
          },
        );
        break;
      case "date":
      case "hour":
        questionWidget = QuestionDate(
          key: UniqueKey(),
          questionData: questionData,
          getAnswer: (dynamic value) {
            response = value;
          },
        );
        break;
      case "number":
        questionWidget = QuestionNumber(
          key: UniqueKey(),
          questionData: questionData,
          getAnswer: (int value) {
            response = value;
          },
        );
        break;
      case "text":
        questionWidget = QuestionText(
          key: UniqueKey(),
          questionData: questionData,
          getAnswer: (String value) {
            response = value;
          },
        );
        break;
      default:
        questionWidget = Container();
        break;
    }
    return Expanded(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            child: AnimatedTextKit(
              key: UniqueKey(),
              animatedTexts: [
                TypewriterAnimatedText(
                  lib
                      .questionGetQuestionText(questionData)
                      .cast<Utf8>()
                      .toDartString(),
                  curve: Curves.easeInOut,
                  textStyle: TextStyle(
                    color: Colors.grey[800],
                  ),
                ),
              ],
              isRepeatingAnimation: false,
            ),
          ),
          SizedBox(height: 3.h),
          questionWidget,
          const Spacer(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 5.h),
              child: ElevatedButton(
                onPressed: () async {
                  if (response == null) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text(Res.pleaseAnswer),
                      ),
                    );
                  } else {
                    nextQuestionStruct? nextQuestion;
                    switch (type) {
                      case "date":
                        nextQuestion = lib.questionAnswerQuestionDate(
                          questionData,
                          (response as DateTime).millisecondsSinceEpoch ~/ 1000,
                          widget.responseData,
                        );
                        break;
                      case "number":
                      case "hour":
                        nextQuestion = lib.questionAnswerQuestionInt(
                          questionData,
                          response,
                          widget.responseData,
                        );
                        break;
                      case "adjective":
                      case "text":
                        var data = lib.questionAnswerQuestionString(
                          questionData,
                          (response as String).toNativeUtf8() as Pointer<Char>,
                          widget.responseData,
                        );
                        nextQuestion = data;
                        break;
                    }
                    if (nextQuestion!.question.address == 0) {
                      lib.responseSave(
                        widget.responseData,
                        (await Res.tmpResponsePath()).toNativeUtf8()
                            as Pointer<Char>,
                      );
                      responseList_t? responses;
                      if (await File(await Res.dbPath()).exists()) {
                        responses = lib.responsesFromFile(
                          (await Res.dbPath()).toNativeUtf8() as Pointer<Char>,
                        );
                      } else {
                        responses = lib.responsesCreate();
                      }
                      lib.responsesAddResponse(responses, widget.responseData);
                      lib.responsesSave(
                        responses,
                        (await Res.dbPath()).toNativeUtf8() as Pointer<Char>,
                      );
                      lib.responsesFree(responses);
                      // ignore: use_build_context_synchronously
                      Navigator.pop(context);
                    } else {
                      lib.responseSave(
                        widget.responseData,
                        (await Res.tmpResponsePath()).toNativeUtf8()
                            as Pointer<Char>,
                      );
                      String anim =
                          nextQuestion.animation.cast<Utf8>().toDartString();
                      lib.freeChar(nextQuestion.animation);
                      widget.callback(anim);
                      setState(() {
                        questionData = nextQuestion!.question.ref;
                      });
                    }
                  }
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Theme.of(context).colorScheme.primary,
                  elevation: 5,
                  fixedSize: Size(20.w, 20.w),
                  padding: const EdgeInsets.all(0),
                ),
                child: Icon(
                  Icons.check_rounded,
                  color: Colors.grey[800],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
