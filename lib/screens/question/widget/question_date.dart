import 'package:ffi/ffi.dart';
import 'package:flutter/material.dart';
import 'package:hatchcare/common/res.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';

/// A widget that displays a button to select a date and/or time.
class QuestionDate extends StatefulWidget {
  /// Constructs a [QuestionDate] widget.
  ///
  /// The [questionData] parameter is the data for the question.
  /// The [getAnswer] parameter is a callback function that will be called with the selected date and/or time.
  const QuestionDate({
    super.key,
    required this.questionData,
    required this.getAnswer,
  });

  /// The data for the question.
  final question_t questionData;

  /// A callback function that will be called with the selected date and/or time.
  final void Function(dynamic) getAnswer;

  @override
  State<QuestionDate> createState() => _QuestionDateState();
}

class _QuestionDateState extends State<QuestionDate> {
  late final bool askHour;
  late final String type;

  @override
  void initState() {
    var precision = lib.questionGetDatePrecision(widget.questionData);
    type = lib
        .questionGetQuestionType(widget.questionData)
        .cast<Utf8>()
        .toDartString();
    askHour = (type == "hour" || precision >= 3);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        DateTime? date;
        if (lib
                .questionGetQuestionType(widget.questionData)
                .cast<Utf8>()
                .toDartString() ==
            "date") {
          date = await showDatePicker(
            context: context,
            firstDate: DateTime.now().subtract(const Duration(days: 14)),
            lastDate: DateTime.now(),
          );
        } else {
          final now = DateTime.now();
          date = DateTime(now.year, now.month, now.day);
        }
        TimeOfDay? time;
        if (askHour && date != null) {
          time = await showTimePicker(
            // ignore: use_build_context_synchronously
            context: context,
            initialTime: TimeOfDay.now(),
          );
        }
        date ??= DateTime.now();
        if (time != null) {
          date = date.copyWith(hour: time.hour, minute: time.minute);
        }

        dynamic data;
        if (type == "hour") {
          data = date.hour * 3600 + date.minute * 60 + date.second;
        } else {
          data = date;
        }
        widget.getAnswer(data);
      },
      child: const Text(Res.selectDate),
    );
  }
}
