import 'package:flutter/material.dart';
import 'package:hatchcare/generated_bindings.dart';
import 'package:hatchcare/main.dart';
import 'package:hatchcare/screens/question/widget/question.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:rive/rive.dart';

/// A page that displays a question and allows the user to provide a response.
class QuestionPage extends StatefulWidget {
  const QuestionPage({super.key, this.questionData, this.reponseData});

  final question_t? questionData;
  final response_t? reponseData;

  @override
  State<QuestionPage> createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  StateMachineController? _controller;

  void _onRiveInit(Artboard artboard) {
    _controller ??= StateMachineController.fromArtboard(artboard, 'hatchip')!;
    artboard.addController(_controller!);
    _controller!.isActive = true;
    _controller!.findInput<bool>("hatched")?.value = true;
  }

  @override
  Widget build(BuildContext context) {
    question_t question = widget.questionData ?? lib.questionGetFirst();
    response_t response = widget.reponseData ?? lib.responseCreate();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.secondary,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 100.w,
            width: 100.w,
            child: RiveAnimation.asset(
              'assets/hatchip.riv',
              fit: BoxFit.fitWidth,
              onInit: _onRiveInit,
            ),
          ),
          Question(
              questionData: question,
              responseData: response,
              callback: (String value) {
                (_controller!.findInput<bool>(value) as SMITrigger).fire();
              }),
        ],
      ),
    );
  }
}
