/**
 * @file grapheQuestions.cpp
 * @brief Implementation file for the grapheQuestions module.
 *
 * This file contains the main function that demonstrates the usage of the grapheQuestions module.
 */
#include "../data/questionDatas.h"

/**
 * @brief Main function that demonstrates the usage of the grapheQuestions module.
 * @return 0
 */
int main() {
    cout << q1.toMermaid() << endl;
    return 0;
}