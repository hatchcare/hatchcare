/**
 * @file correlation.h
 * @brief This file contains the declarations of functions and structures related to correlation calculations.
 */

#pragma once

#ifdef __cplusplus

#include "../reponse/Correlation.h"
#include "../question/QuestionType.h"
#include <cstring>

#ifdef WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT extern "C" __attribute__((visibility("default"))) __attribute__((used))
#endif

extern "C"
{

#else
#include <stdbool.h>
#define EXPORT
#endif

/**
 * @struct correlation
 * @brief Structure representing a correlation object.
 */
struct correlation {
    void *obj;
};
typedef struct correlation correlation_t;

/**
 * @brief Frees the memory allocated for a correlation object.
 * @param correlation The correlation object to be freed.
 */
EXPORT
void correlationFree(correlation_t correlation);

/**
 * @struct correlationList
 * @brief Structure representing a list of correlation objects.
 */
struct correlationList {
    correlation_t *correlations; /**< Array of correlation objects */
    int size;                    /**< Number of correlation objects in the list */
};
typedef struct correlationList correlationList_t;

/**
 * @brief Frees the memory allocated for a correlation list object.
 * @param correlationList The correlation list object to be freed.
 */
EXPORT
void correlationListFree(correlationList_t correlationList);

/**
 * @brief Gets the question type A of a correlation.
 * @param correlation The correlation object.
 * @return The question type A as a null-terminated string.
 */
EXPORT
char *correlationGetQuestionTypeA(correlation_t correlation);

/**
 * @brief Gets the question type B of a correlation.
 * @param correlation The correlation object.
 * @return The question type B as a null-terminated string.
 */
EXPORT
char *correlationGetQuestionTypeB(correlation_t correlation);

/**
 * @brief Checks if a correlation is an adjective.
 * @param correlation The correlation object.
 * @return True if the correlation is an adjective, false otherwise.
 */
EXPORT
bool correlationGetIsAdjective(correlation_t correlation);

/**
 * @brief Gets the adjective category of a correlation.
 * @param correlation The correlation object.
 * @return The adjective category as a null-terminated string.
 */
EXPORT
char *correlationGetAdjectiveCategory(correlation_t correlation);

/**
 * @brief Gets the coefficient of a correlation.
 * @param correlation The correlation object.
 * @return The coefficient value.
 */
EXPORT
double correlationGetCoefficient(correlation_t correlation);

/**
 * @brief Gets the lag of a correlation.
 * @param correlation The correlation object.
 * @return The lag value.
 */
EXPORT
int correlationGetLag(correlation_t correlation);

#ifdef __cplusplus
}
#endif
