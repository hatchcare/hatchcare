#include "reponse.h"

response_t responseCreate() {
    auto *obj = new Reponse();
    auto rep = responseStruct();
    rep.obj = obj;
    return rep;
}

[[maybe_unused]]  void reponseFree(response_t reponse) {
    auto obj = (Reponse *) reponse.obj;
    delete obj;
}

void responseSave(response_t reponse, const char *filename) {
    auto obj = (Reponse *) reponse.obj;
    obj->writeToFile(string(filename));
}
