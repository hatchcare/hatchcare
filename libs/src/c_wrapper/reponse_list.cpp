#include "reponse_list.h"


[[maybe_unused]]  void responsesFree(responseList_t reponse) {
    auto obj = (ReponseList *) reponse.obj;
    delete obj;
}

[[maybe_unused]] void stringArrayFree(stringArray_t stringArray) {
    auto a = (char **) stringArray.array;
    for (int i = 0; i < stringArray.len; i++) {
        delete[] a[i];
    }
    delete[] a;
}

[[maybe_unused]]  void intListFree(intList_t intList) {
    delete[] intList.data;
}

[[maybe_unused]] void responsesAddResponse(responseList_t responses, response_t reponse) {
    auto obj = (ReponseList *) responses.obj;
    auto rep = (Reponse *) reponse.obj;
    obj->addReponse(*rep);
}

[[maybe_unused]] void responsesSave(responseList_t reponse, const char *filename) {
    auto obj = (ReponseList *) reponse.obj;
    auto result = obj->saveToFile(string(filename));
    if (!result) {
        throw runtime_error("Failed to save the response list to the file.");
    }
}


[[maybe_unused]] responseList_t responsesCreate() {
    auto *obj = new ReponseList();
    auto rep = responseList_t();
    rep.obj = obj;
    return rep;
}

[[maybe_unused]] responseList_t responsesFromFile(const char *filename) {
    auto rep = responseList_t();
    auto *obj = new ReponseList(ReponseList::fromFile(string(filename)));
    rep.obj = obj;
    return rep;
}

[[maybe_unused]] void longListFree(longList_t longList) {
    delete[] longList.data;
}

[[maybe_unused]] longList_t responsesGetDate(responseList_t response) {
    auto reponseList = (ReponseList *) response.obj;
    auto res = reponseList->getDateList();
    auto *data = new long[res.size()];
    for (int i = 0; i < res.size(); ++i) {
        data[i] = res[i];
    }
    longList_t longData;
    longData.size = (int) res.size();
    longData.data = data;
    return longData;
}

[[maybe_unused]] intList_t responsesGetIntArray(responseList_t reponse, char *key) {
    auto reponseList = (ReponseList *) reponse.obj;
    string keyStr(key);
    vector<optional<int>> res;
    for (auto i: QuestionType::values) {
        if (keyStr == QuestionType::to_string(i)) {
            res = reponseList->getIntList(i);
            break;
        }
    }
    //clean the vector
    vector<int> cleanRes;
    for (auto i: res) {
        if (i.has_value()) {
            cleanRes.push_back(i.value());
        }
    }

    intList_t data;
    data.size = (int) cleanRes.size();
    data.data = new int[data.size];
    for (int i = 0; i < data.size; ++i) {
        data.data[i] = cleanRes[i];
    }

    return data;
}

[[maybe_unused]] stringArray_t responsesGetStringArray(responseList_t reponse, char *key) {
    auto reponseList = (ReponseList *) reponse.obj;
    string keyStr(key);
    vector<optional<basic_string<char>>> res;
    for (auto i: QuestionType::values) {
        if (keyStr == QuestionType::to_string(i)) {
            res = reponseList->getStringList(i);
            break;
        }
    }
    //clean the vector
    vector<string> cleanRes;
    for (auto i: res) {
        if (i.has_value()) {
            cleanRes.emplace_back(i.value());
        }
    }

    char **data = (char **) malloc(sizeof(char *) * cleanRes.size());
    for (int i = 0; i < cleanRes.size(); ++i) {
        data[i] = (char *) malloc(sizeof(char) * (cleanRes[i].size() + 1));
        strcpy(data[i], cleanRes[i].c_str());
    }

    stringArray_t stringData;
    stringData.len = (int) cleanRes.size();
    stringData.array = data;

    return stringData;
}

[[maybe_unused]] correlationList_t responsesGetCorrelationArray(responseList_t reponse) {
    auto reponseList = (ReponseList *) reponse.obj;
    auto res = reponseList->findCorrelation();
    cout << "Correlation size: " << res.size() << endl;
    auto *correlations = new correlation_t[res.size()];
    for (int i = 0; i < res.size(); ++i) {
        correlations[i] = {new Correlation(res[i])};
    }
    correlationList_t correlationData;
    correlationData.size = (int) res.size();
    correlationData.correlations = correlations;

    return correlationData;
}