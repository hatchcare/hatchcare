import os

files = os.listdir("./")
files = [file for file in files if file.endswith(".h")]
output = []
for file in files:
    with open(file, "r") as f:
        lines = f.readlines()
        for i in range(len(lines)):
            if lines[i].startswith("EXPORT"):
                i += 1
                data = lines[i].strip().split(" ")
                data = data[1]
                data = data.split("(")
                data = data[0]
                if data.startswith("*"):
                    data = data[1:]
                output.append(data)

for line in output:
    print(line)
