#include "hatchcare_lib.h"


[[maybe_unused]] question_t questionGetFirst() {
    auto questionData = question_t();
    questionData.obj = (void *) &q1;
    return questionData;
}

[[maybe_unused]] void nextQuestionFree(nextQuestion_t nextQuestion) {
    auto q = (Question *) nextQuestion.question;
    auto a = (char *) nextQuestion.animation;
    delete q;
    delete[] a;
}

[[maybe_unused]] char *questionGetQuestionType(question_t question) {
    auto q = (Question *) question.obj;

    string str = q->getReponseType();
    char *cString = new char[str.size() + 1];
    strcpy(cString, str.c_str());
    return cString;
}

[[maybe_unused]] char *questionGetQuestionText(question_t question) {
    auto q = (Question *) question.obj;

    string str = q->questionText;
    char *cstr = new char[str.size() + 1];
    strcpy(cstr, str.c_str());
    return cstr;
}

[[maybe_unused]] nextQuestion_t questionAnswerQuestionInt(question_t question, int answer, response_t reponse) {
    return questionAnswerQuestion(question, reponseTypes(answer), reponse);
}

nextQuestion_t questionAnswerQuestionString(question_t question, char *answer, response_t reponse) {
    string answerString = string(answer);
    return questionAnswerQuestion(question, reponseTypes(answerString), reponse);
}

[[maybe_unused]] nextQuestion_t questionAnswerQuestionDate(question_t question, int answer, response_t reponse) {
    return questionAnswerQuestion(question, reponseTypes((time_t) answer), reponse);
}

[[maybe_unused]] bool is_null_pointer(question_t *question) {
    return question == nullptr;
}

[[maybe_unused]] stringArray_t adjectiveGet() {
    auto adj = adjectivesList;
    char **adjectivesArray = new char *[ADJECTIVES_SIZE];
    int i = 0;
    for (const auto &adjective: adj) {
        adjectivesArray[i] = new char[adjective.size() + 1];
        strcpy(adjectivesArray[i], adjective.c_str());
        i++;
    }
    return {adjectivesArray, ADJECTIVES_SIZE};
}

[[maybe_unused]] stringArray_t adjectivesGetSub(char *adjective) {
    auto adjectiveString = string(adjective);
    auto subAdjectives = adjectivesSubList.at(adjectiveString);
    char **subAdjectivesArray = new char *[subAdjectives.size()];
    int i = 0;
    for (const auto &subAdjective: subAdjectives) {
        subAdjectivesArray[i] = new char[subAdjective.size() + 1];
        strcpy(subAdjectivesArray[i], subAdjective.c_str());
        i++;
    }
    return {subAdjectivesArray, (int) subAdjectives.size()};
}

[[maybe_unused]] int questionGetDatePrecision(question_t question) {
    auto q = (QuestionDate *) question.obj;
    return q->getPrecision();
}

[[maybe_unused]] void freeChar(const char *tabChar) {
    delete[] tabChar;
}

[[maybe_unused]] int questionGetHourPrecision(question_t question) {
    auto q = (QuestionHour *) question.obj;
    return q->getPrecision();
}

[[maybe_unused]] int questionNumberGetMax(question_t question) {
    auto q = (QuestionNumber *) question.obj;
    return q->max;
}

[[maybe_unused]] int questionNumberGetMin(question_t question) {
    auto q = (QuestionNumber *) question.obj;
    return q->min;
}

[[maybe_unused]] int questionNumberGetStep(question_t question) {
    auto q = (QuestionNumber *) question.obj;
    return q->step;
}

#ifdef __cplusplus

nextQuestion_t questionAnswerQuestion(question_t question, reponseTypes answer, response_t reponse) {
    auto q = (Question *) question.obj;
    auto r = (Reponse *) reponse.obj;
    auto nextQuestion = q->saveReponse(r, std::move(answer));
    if (nextQuestion.has_value()) {
        auto questionData = nextQuestion_t();
        questionData.question = new question_t();
        questionData.question->obj = (void *) nextQuestion.value().first.get();
        auto nextAnim = new char[AnimationTriggers::to_string(nextQuestion.value().second).size() + 1];
        strcpy(nextAnim, AnimationTriggers::to_string(nextQuestion.value().second).data());
        questionData.animation = nextAnim;
        return questionData;
    } else {
        return nextQuestion_t{nullptr, nullptr};
    }
}

#endif
