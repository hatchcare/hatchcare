/**
 * @file reponse_list.h
 * @brief This file contains the declarations of functions and structures for the response list.
 */

#pragma once

#include "reponse.h"
#include "correlation.h"

#ifdef __cplusplus

#include "../reponse/Correlation.h"

#ifdef WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT extern "C" __attribute__((visibility("default"))) __attribute__((used))
#endif

extern "C"
{

#else
#include <stdbool.h>
#define EXPORT
#endif

/**
 * @brief Structure representing a list of responses.
 */
struct responseListStruct {
    void *obj;
};
typedef struct responseListStruct responseList_t;

/**
 * @brief Frees the memory allocated for a response list.
 *
 * @param reponse The response list to free.
 */
EXPORT
void responsesFree(responseList_t reponse);

/**
 * @brief Structure representing a list of integers.
 */
struct intListStruct {
    int *data;
    int size;
};
typedef struct intListStruct intList_t;

/**
 * @brief Frees the memory allocated for an integer list.
 *
 * @param intList The integer list to free.
 */
EXPORT
void intListFree(intList_t intList);

/**
 * @brief Structure representing a list of long integers.

 */
struct longList {
    long *data;
    int size;
};
typedef struct longList longList_t;

/**
 * @brief Frees the memory allocated for a long integer list.
 *
 * @param longList The long integer list to free.
 */
EXPORT
void longListFree(longList_t longList);

/**
 * @brief Retrieves the date of the responses.
 *
 * @param reponse The response.
 * @return The date of the response.
 */
EXPORT
longList_t responsesGetDate(responseList_t reponse);

/**
 * @brief Structure representing an array of strings.
 */
struct stringArrayStruct {
    char **array;
    int len;
};
typedef struct stringArrayStruct stringArray_t;

/**
 * @brief Frees the memory allocated for a string array.
 *
 * @param stringArray The string array to free.
 */
EXPORT
void stringArrayFree(stringArray_t stringArray);

/**
 * @brief Adds a response to the response list.
 *
 * @param responses The response list.
 * @param reponse The response to add.
 */
EXPORT
void responsesAddResponse(responseList_t responses, response_t reponse);

/**
 * @brief Saves the response list to a file.
 *
 * @param reponse The response list to save.
 * @param filename The name of the file to save to.
 */
EXPORT
void responsesSave(responseList_t reponse, const char *filename);

/**
 * @brief Creates a new response list.
 *
 * @return The newly created response list.
 */
EXPORT
responseList_t responsesCreate();

/**
 * @brief Loads a response list from a file.
 *
 * @param filename The name of the file to load from.
 * @return The loaded response list.
 */
EXPORT
responseList_t responsesFromFile(const char *filename);

/**
 * @brief Retrieves an integer array from a response.
 *
 * @param reponse The response.
 * @param key The key of the integer array.
 * @return The integer array.
 */
EXPORT
intList_t responsesGetIntArray(responseList_t reponse, char *key);

/**
 * @brief Retrieves a string array from a response.
 *
 * @param reponse The response.
 * @param key The key of the string array.
 * @return The string array.
 */
EXPORT
stringArray_t responsesGetStringArray(responseList_t reponse, char *key);

/**
 * @brief Retrieves a correlation array from a response.
 *
 * @param reponse The response.
 * @return The correlation array.
 */
EXPORT
correlationList_t responsesGetCorrelationArray(responseList_t reponse);

#ifdef __cplusplus
}
#endif
