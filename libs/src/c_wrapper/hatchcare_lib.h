/**
 * @file hatchcare_lib.h
 * @brief This file contains the declarations of functions and structures for the Hatchcare library.
 */


#include "reponse.h"
#include "reponse_list.h"
#include "correlation.h"

#ifdef __cplusplus

#include "../../data/questionDatas.h"
#include "../../src/question/QuestionAdjective.h"
#include "../../src/question/QuestionDate.h"
#include "../../src/question/QuestionHour.h"
#include "../../src/question/Question.h"
#include <cstring>
#include <utility>


#ifdef WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT extern "C" __attribute__((visibility("default"))) __attribute__((used))
#endif

extern "C" {

#else
#include <stdbool.h>
#define EXPORT
#endif

/**
 * @struct questionStruct
 * @brief Structure representing a question object.
 */
struct questionStruct {
    void *obj;
};
typedef struct questionStruct question_t;

/**
 * @struct nextQuestionStruct
 * @brief Structure representing the next question and animation.
 */
struct nextQuestionStruct {
    question_t *question;
    char *animation;
};
typedef struct nextQuestionStruct nextQuestion_t;

/**
 * @brief Frees the memory allocated for a nextQuestion object.
 * @param nextQuestion The nextQuestion object to free.
 */
EXPORT
void nextQuestionFree(nextQuestion_t nextQuestion);

/**
 * @brief Free a char array
 */
EXPORT
void freeChar(const char *tabChar);
/**
 * @brief Gets the first question.
 * @return The first question.
 */
EXPORT
question_t questionGetFirst();

/**
 * @brief Gets the question type.
 * @param question The question object.
 * @return The question type.
 */
EXPORT
char *questionGetQuestionType(question_t question);

/**
 * @brief Gets the question text.
 * @param question The question object.
 * @return The question text.
 */
EXPORT
char *questionGetQuestionText(question_t question);

/**
 * @brief Gets the date precision of a question.
 * @param question The question object.
 * @return The date precision.
 */
EXPORT
int questionGetDatePrecision(question_t question);

/**
 * @brief Answers a question with an integer answer.
 * @param question The question object.
 * @param answer The integer answer.
 * @param reponse The response object.
 * @return The next question and animation.
 */
EXPORT
nextQuestion_t questionAnswerQuestionInt(question_t question, int answer, response_t reponse);
/**
 * @brief Gets the hour precision of a question.
 * @param question The question object.
 * @return The hour precision.
 */
EXPORT
int questionGetHourPrecision(question_t question);

/**
 * @brief Answers a question with a string answer.
 * @param question The question object.
 * @param answer The string answer.
 * @param reponse The response object.
 * @return The next question and animation.
 */
EXPORT
nextQuestion_t questionAnswerQuestionString(question_t question, char *answer, response_t reponse);

/**
 * @brief Answers a question with a date answer.
 * @param question The question object.
 * @param answer The date answer.
 * @param reponse The response object.
 * @return The next question and animation.
 */
EXPORT
nextQuestion_t questionAnswerQuestionDate(question_t question, int answer, response_t reponse);

/**
 * @brief Gets the list of adjectives.
 * @return The list of adjectives.
 */
EXPORT
stringArray_t adjectiveGet();

/**
 * @brief Gets the sub-adjectives for a given adjective.
 * @param adjective The adjective.
 * @return The list of sub-adjectives.
 */
EXPORT
stringArray_t adjectivesGetSub(char *adjective);

/**
 * @brief Get the max entered number possible for a question
 * @param question The question object.
 * @return The max number.
 */
EXPORT
int questionNumberGetMax(question_t question);

/**
 * @brief Get the min entered number possible for a question
 * @param question The question object.
 * @return The min number.
 */
EXPORT
int questionNumberGetMin(question_t question);


/**
 * @brief Get the step entered number possible for a question
 * @param question The question object.
 * @return The step number.
 */
EXPORT
int questionNumberGetStep(question_t question);

#ifdef __cplusplus
/**
 * @brief Answers a question with a response type.
 * @param question The question object.
 * @param answer The response type.
 * @param reponse The response object.
 * @return The next question and animation.
 */
nextQuestion_t questionAnswerQuestion(question_t question, reponseTypes answer, response_t reponse);

}
#endif
