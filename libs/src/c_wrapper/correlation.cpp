#include "correlation.h"


char *correlationGetQuestionTypeA(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    char *questionTypeA = (char *) malloc(QuestionType::to_string(obj->questionTypeA).size() + 1);
    strcpy(questionTypeA, QuestionType::to_string(obj->questionTypeA).c_str());
    return questionTypeA;
}

char *correlationGetQuestionTypeB(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    char *questionTypeB = (char *) malloc(QuestionType::to_string(obj->questionTypeB).size() + 1);
    strcpy(questionTypeB, QuestionType::to_string(obj->questionTypeB).c_str());
    return questionTypeB;
}

bool correlationGetIsAdjective(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    return obj->adjectiveCategory.has_value();
}

char *correlationGetAdjectiveCategory(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    if (obj->adjectiveCategory.has_value()) {
        char *adjectiveCategory = (char *) malloc(obj->adjectiveCategory.value().size() + 1);
        strcpy(adjectiveCategory, obj->adjectiveCategory.value().c_str());
        return adjectiveCategory;
    }
    return nullptr;
}

double correlationGetCoefficient(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    return obj->coefficient;
}

int correlationGetLag(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    return obj->lag;
}

void correlationFree(correlation_t correlation) {
    auto obj = (Correlation *) correlation.obj;
    delete obj;
}

void correlationListFree(correlationList_t correlationList) {
    for (int i = 0; i < correlationList.size; i++) {
        correlationFree(correlationList.correlations[i]);
    }
    free(correlationList.correlations);
}