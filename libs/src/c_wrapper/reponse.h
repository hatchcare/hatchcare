/**
 * @file
 * @brief The file contains the declaration of the Reponse class.
 */

#pragma once

#ifdef __cplusplus

#include "../reponse/Reponse.h"
#include "../reponse/ReponseList.h"
#include <cstring>

#ifdef WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT extern "C" __attribute__((visibility("default"))) __attribute__((used))
#endif

/**
 * @brief C wrapper for response operations.
 *
 * This header file defines the C interface for working with response objects.
 * It provides functions for creating, freeing, and saving response objects.
 */

extern "C"
{
#else
#define EXPORT
#endif


/**
 * @brief Structure representing a response object.
 *
 * This structure holds a pointer to the response object.
 */
struct responseStruct {
    void *obj;
};
typedef struct responseStruct response_t;

/**
 * @brief Frees the memory allocated for a response object.
 *
 * This function frees the memory allocated for the given response object.
 *
 * @param reponse The response object to free.
 */
EXPORT
void reponseFree(response_t reponse);

/**
 * @brief Creates a new response object.
 *
 * This function creates a new response object and returns it.
 *
 * @return The newly created response object.
 */
EXPORT
response_t responseCreate();

/**
 * @brief Saves a response object to a file.
 *
 * This function saves the given response object to the specified file.
 *
 * @param reponse The response object to save.
 * @param filename The name of the file to save the response to.
 */
EXPORT
void responseSave(response_t reponse, const char *filename);

#ifdef __cplusplus
}
#endif
