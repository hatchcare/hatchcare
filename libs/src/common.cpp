/**
 * @file common.cpp
 * @brief Implementation file for the common functions.
 */

#include <iomanip>
#include "common.h"


/**
 * @brief Displays the given text character by character with a delay.
 *
 * @param txt The text to be displayed.
 */
void displayText(string txt) {
    int l = (int) txt.length();
    for (int i = 0; i < l; i++) {
        cout << txt[i] << flush;
        sleep_for(milliseconds(15));
    }
}

string dateToString(const time_t &date) {
    char buffer[32];
    tm *ptm = localtime(&date);
    strftime(buffer, 32, "%a, %d.%m.%Y %H:%M:%S", ptm);
    return buffer;
}

string reblaceAll(string str, const string &from, const string &to) {
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
    return str;
}