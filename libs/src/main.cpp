/**
 * @file main.cpp
 * @brief Main file for the HatchCare application.
 */

#include <iostream>
#include <fstream>
#include "question/Question.h"
#include "../data/questionDatas.h"
#include "common.h"
#include "reponse/ReponseList.h"
#include "reponse/Graph.h"


/**
 * @brief Adds a response to the application.
 */
void addResponse() {
    auto *reponse = new Reponse;
    optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> q = pair(make_shared<QuestionAdjective>(q1),
                                                                           AnimationTriggers::to_happy);
    while (q.has_value()) {
        q = q.value().first->askConsole(reponse);
    }
    displayText("Merci pour tes réponses ! \n");

    ReponseList reponseList;
    ifstream file("db.csv");
    if (file) {
        file.close();
        reponseList = ReponseList::fromFile("db.csv");
    }
    reponseList.addReponse(*reponse);
    auto status = reponseList.saveToFile("db.csv");
    if (status) {
        displayText("Tes réponses ont bien été enregistrées ! \n");
    } else {
        displayText("Oh non! Quelque chose s'est mal passé lors de l'enregistrement de tes réponses. \n");
    }
}

/**
 * @brief Displays the tutorial messages and prompts the user to start or quit.
 *
 * If the user chooses to start, it calls the ajouterReponse() function.
 */
void tutoriel() {
    cout << endl
         << endl;
    displayText("Whoa, sois le bienvenu ! Je suis Hatchip, le programme chargé de te guider!\n \n");
    displayText("HatchCare, c'est une application pour t'aider à prendre soin de toi.\n");
    displayText(
            "Ici, tu pourras consigner ton ressenti et tes habitudes, de jour en jour, les consulter et compléter.\n");
    displayText("Comme ça, tu pourras voir comment tu t'en sortais, et trouver des moyens d'aller mieux !\n");
    displayText(
            "Et puis si tu ne t'en sens pas encore capable, on est là pour t'aiguiller en fonction de tes réponses !\n \n");
    displayText("Alors ? On commence ? \n");
    displayText("(Entrez n'importe quelle number pour poursuivre.) \n");
    cout << ANSWER_ARROW;
    int reponse;
    cin >> reponse;
    displayText("\n     Allons-y !! \n");
    addResponse();
    displayText("\n\n     Tout s'est bien passé ? \n");
    displayText(
            "Maintenant que tu as complété la première réponse, pouquoi ne pas découvrir le reste de l'application ? \n");
}

void afficherDesBeauGraphes() {
    cout << "Voici un beau graphe" << endl;
    vector<vector<pair<float, float>>> data = {
            {{1,  2},  {3,  4},  {5,  6}, {7, 8}, {9, 10}, {11, 12}, {13, 14}, {15, 16}, {17, 18}, {19, 20}, {21, 22}, {23, 24}},
            {{-1, -2}, {-3, -4}, {-5, -6}}};
    Graph graph(data, Correlation(QuestionType::Type::mealQuality, QuestionType::Type::noteMood, nullopt, 1.0, 0));
    cout << graph << endl;
}

void findCorrelations() {
    auto reponseList = ReponseList::fromFile("db.csv");
    auto CorrelationList = reponseList.findCorrelation();
    for ([[maybe_unused]] auto i: CorrelationList) {
        displayText("On dirait qu'il y a correlation entre ");
//        cout << QuestionType::to_string(i.questionTypeA) << " et " << QuestionType::to_string(i.questionTypeB) << "avec un coefficient de similarité " << " valant " << i.coefficient << "." << endl;
    }
}

/**
 * @brief The main function of the HatchCare application.
 *
 * It prompts the user to choose between starting the tutorial or adding a response.
 *
 * @return 0 if the program executes successfully.
 */
int main() {
    displayText("...... Bienvenue sur HatchCare !\n");
    int n = -1;
    bool exit = false;
    displayText(
            "Si c'est la première fois que vous utilisez HatchCare, entrez 1. Sinon, entrez n'importe quel numéro.\n");
    cout << ANSWER_ARROW;
    cin >> n;
    cout << endl;
    if (n == 1) {
        tutoriel();
    } else {
        displayText("Content de te revoir ! \n \n \n");
    }
    do {
        n = -1;
        displayText("\n\n\n     Alors, comment vas-tu en ce moment ? \n");
        displayText("(Si tu veux rajouter une réponse, entre 1.)\n");
        displayText("(Si tu veux consulter tes réponses précédentes, entre 2.)\n");
        displayText("(Et si tu veux voir l'analyse actuelle de tes réponses, entre 3.)\n");
        displayText("(Pour quitter Hatchcare, entrez 0.)\n");
        cout << ANSWER_ARROW;
        cin >> n;
        switch (n) {
            case 0:
                displayText("A la prochaine fois ! \n.\n.\n.\n");
                exit = true;
                break;
            case 1:
                addResponse();
                break;
            case 2:
                afficherDesBeauGraphes();
                break;
            case 3:
                findCorrelations();
                break;
            default:
                displayText("Oh non! Entrée invalide.");
                break;
        }
    } while (!exit);
    return 0;
}