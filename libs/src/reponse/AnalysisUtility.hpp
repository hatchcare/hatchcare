/**
 * @file AnalysisUtility.hpp
 * @brief Contains the declaration of the AnalysisUtility class.
 */
#ifndef HATCHCARE_ANALYSIS_UTILITY_HPP
#define HATCHCARE_ANALYSIS_UTILITY_HPP

#include <vector>
#include <map>
#include <algorithm>
#include <valarray>

using namespace std;

/**
 * @class AnalysisUtility
 * @brief Provides utility functions for data analysis.
 */
class AnalysisUtility {
public:
    /**
     * @brief Ranks the data in descending order and returns a vector of pairs with the data and their corresponding rank.
     * @tparam T The type of the data.
     * @param data The input data.
     * @return A vector of pairs with the data and their rank.
     */
    template<typename T>
    static vector<pair<T, double>> rankData(const vector<T> &data) {
        vector<int> listSorted(data);
        std::sort(listSorted.begin(), listSorted.end(), greater());
        // create a map of the rank of the values
        map<int, vector<int>> rank;
        for (int j = 0; j < data.size(); ++j) {
            rank[listSorted[j]].push_back(j + 1);
        }
        // calculate the rank of the values
        map<int, double> rankValue;
        for (int j = 0; j < data.size(); ++j) {
            double tmpRank = 0;
            for (auto &r: rank[data.at(j)]) {
                tmpRank += r;
            }
            tmpRank /= (double) (rank[data.at(j)].size());
            rankValue[data.at(j)] = tmpRank;
        }
        // create a list of the values with their rank
        vector<pair<T, double>> listWithRank(data.size());
        for (int j = 0; j < data.size(); ++j) {
            listWithRank[j] = make_pair(data[j], rankValue[data.at(j)]);
        }
        return listWithRank;
    }

    /**
     * @brief Calculates the Spearman correlation coefficient between two lists of data.
     * @tparam T The type of the data.
     * @param listA The first list of data.
     * @param listB The second list of data.
     * @return The Spearman correlation coefficient.
     */
    template<typename T>
    static double spearmanCorrelation(const vector<T> &listA, const vector<T> &listB) {
        // let's calculate the rank of the values
        vector<pair<int, double>> rankedListA = AnalysisUtility::rankData(listA);
        vector<pair<int, double>> rankedListB = AnalysisUtility::rankData(listB);

        int size = (int) rankedListA.size();

        // rank difference
        vector<double> diffRank;
        for (int j = 0; j < size; ++j) {
            diffRank.push_back(rankedListA[j].second - rankedListB[j].second);
        }

        // difference squared ranks
        double sumDiffRankSquared = 0;
        for (int j = 0; j < size; ++j) {
            sumDiffRankSquared += pow(diffRank[j], 2);
        }

        // Calculate Spearman correlation coefficient
        return 1 - (6 * sumDiffRankSquared) / (pow(size, 3) - size);
    }

    static constexpr double maxDataLoosePercent = 0.1; // 0.25 equivalent to lose 1/4 of the data

    /**
     * @brief Finds the best correlation between two integer data sets.
     * This method prioritizes the minimum lag if there are two correlations with the same value.
     * @param data1 The first data set.
     * @param data2 The second data set.
     * @return An optional pair of the best correlation and the best lag.
     */
    static optional<pair<double, int>> findBestCorrelation(const vector<int> &data1, const vector<int> &data2) {
        // Check if the lists are of equal size
        if (data1.size() != data2.size() || (data1.empty() || data2.empty())) {
            // Return an empty optional if lists are of different sizes
            return nullopt;
        }

        // calculate the correlations

        int size = (int) data1.size();
        int bestLag = 0;
        double bestCorrelation = 0;

        // loop to find the best lag
        int maxLag = (int) round((AnalysisUtility::maxDataLoosePercent) * size);
        for (int i = -maxLag; i <= maxLag; ++i) {
            // create a copy of the list
            auto listAWithLag = vector<int>(size - abs(i));
            auto listBWithLag = vector<int>(size - abs(i));
            // shift the list by i
            for (int j = 0; j < size - abs(i); ++j) {
                if (i < 0) {
                    listAWithLag[j] = data1[j + abs(i)];
                    listBWithLag[j] = data2[j];
                } else {
                    listAWithLag[j] = data1[j];
                    listBWithLag[j] = data2[j + i];
                }
            }

            // calculate the correlation
            double correlation = AnalysisUtility::spearmanCorrelation(listAWithLag, listBWithLag);

            // if the correlation is better than the best correlation found so far
            // and the lag is smaller than the best lag found so far
            // update the best correlation and the best lag
            if (abs(correlation) > abs(bestCorrelation) ||
                (abs(i) < abs(bestLag) && abs(correlation) == abs(bestCorrelation))) {
                bestCorrelation = correlation;
                bestLag = i;
            }
        }
        // return the best correlation found
        return make_pair(bestCorrelation, bestLag);
    }
};

#endif // HATCHCARE_ANALYSIS_UTILITY_HPP
