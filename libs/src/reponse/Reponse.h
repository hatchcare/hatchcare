/**
 * @file Reponse.h
 * @brief Header file for the Reponse class.
 */

#ifndef HATCHCARE_REPONSE_H
#define HATCHCARE_REPONSE_H

#include <string>
#include <optional>
#include <ostream>
#include <fstream>
#include "../common.h"

using namespace std;


#ifndef REPONSE_H
#define REPONSE_H

#include <iostream>
#include <optional>
#include <string>

/**
 * @brief The Reponse class represents a response object.
 *
 * This class stores various optional attributes related to a response, such as mood rating, adjective, rant, sleep details, meal details, etc.
 * It provides methods to compare responses, convert responses to CSV format, and create responses from CSV format.
 */
class Reponse {
public:

    /**
     * @brief Constructs a new Reponse object.
     * @param date The date of the response.
     * @param noteMood The mood rating (optional).
     * @param adjective The adjective (optional).
     * @param rant The rant (optional).
     * @param sleepBegin The start time of sleep (optional).
     * @param sleepEnd The end time of sleep (optional).
     * @param sleepQuality The sleep quality rating (optional).
     * @param sleepPlus Additional sleep details (optional).
     * @param mealHour The meal time (optional).
     * @param mealQuality The meal quality rating (optional).
     * @param mealPlus Additional meal details (optional).
     */
    Reponse(time_t date, const optional<int> &noteMood, const optional<string> &adjective,
            const optional<string> &rant, const optional<int> &sleepBegin,
            const optional<int> &sleepEnd, const optional<int> &sleepQuality,
            const optional<string> &sleepPlus, const optional<int> &mealHour,
            const optional<int> &mealQuality, const optional<string> &mealPlus);

    /**
     * @brief Default constructor for Reponse objects.
     */
    Reponse();

    /**
     * @brief Destructor for Reponse objects.
     */
    ~Reponse();

    /**
     * @brief Converts the response object to CSV format.
     * @return The response object in CSV format.
     */
    [[nodiscard]] string toCSV() const;

    void writeToFile(const string &filename) const;

    /**
     * @brief Creates a response object from CSV format.
     * @param csv The response object in CSV format.
     * @return The created response object.
     */
    static Reponse fromCSV(string csv);

    /**
     * @brief Friend function to overload the output stream operator.
     * @param os The output stream.
     * @param reponse The response object to output.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const Reponse &reponse);

    bool operator==(const Reponse &rhs) const;

    bool operator!=(const Reponse &rhs) const;

    time_t date{};              ///< The date of the response
    optional<int> noteMood;     ///< The mood rating
    optional<string> adjective; ///< The adjective
    optional<string> rant;      ///< The rant
    optional<int> sleepBegin;   ///< The start time of sleep
    optional<int> sleepEnd;     ///< The end time of sleep
    optional<int> sleepQuality; ///< The sleep quality rating
    optional<string> sleepPlus; ///< Additional sleep details
    optional<int> mealHour;     ///< The meal time
    optional<int> mealQuality;  ///< The meal quality rating
    optional<string> mealPlus;  ///< Additional meal details

private:
    bool saved = false;
};

#endif // REPONSE_H

#endif // HATCHCARE_REPONSE_H