/**
 * @file Reponse.cpp
 * @brief Implementation file for the Reponse class.
 */
#include "Reponse.h"

#include <iostream>
#include <chrono>

Reponse::Reponse(time_t date, const optional<int> &noteMood, const optional<string> &adjective,
                 const optional<string> &rant, const optional<int> &sleepBegin,
                 const optional<int> &sleepEnd, const optional<int> &sleepQuality,
                 const optional<string> &sleepPlus, const optional<int> &mealHour,
                 const optional<int> &mealQuality, const optional<string> &mealPlus) : date(date), noteMood(noteMood),
                                                                                       adjective(adjective), rant(rant),
                                                                                       sleepBegin(sleepBegin),
                                                                                       sleepEnd(sleepEnd),
                                                                                       sleepQuality(sleepQuality),
                                                                                       sleepPlus(sleepPlus),
                                                                                       mealHour(mealHour),
                                                                                       mealQuality(mealQuality),
                                                                                       mealPlus(mealPlus) {}

Reponse::Reponse() {
    date = chrono::system_clock::to_time_t(chrono::system_clock::now());
    noteMood = nullopt;
    adjective = nullopt;
    rant = nullopt;
    sleepBegin = nullopt;
    sleepEnd = nullopt;
    sleepQuality = nullopt;
    sleepPlus = nullopt;
    mealHour = nullopt;
    mealQuality = nullopt;
    mealPlus = nullopt;
}

Reponse::~Reponse() = default;


string Reponse::toCSV() const {
    string result;
    result += to_string(date) + ",";
    result += ((noteMood.has_value()) ? to_string(noteMood.value()) : "null") + ",";
    result += ((adjective.has_value()) ? adjective.value() : "null") + ",";
    result += reblaceAll((rant.has_value()) ? rant.value() : "null", ",", "\\,") + ",";
    result += ((sleepBegin.has_value()) ? to_string(sleepBegin.value()) : "null") + ",";
    result += ((sleepEnd.has_value()) ? to_string(sleepEnd.value()) : "null") + ",";
    result += reblaceAll(sleepQuality.has_value() ? to_string(sleepQuality.value()) : "null", ",", "\\,") + ",";
    result += reblaceAll((sleepPlus.has_value() ? sleepPlus.value() : "null"), ",", "\\,") + ",";
    result += ((mealHour.has_value()) ? to_string(mealHour.value()) : "null") + ",";
    result += ((mealQuality.has_value()) ? to_string(mealQuality.value()) : "null") + ",";
    result += reblaceAll((mealPlus.has_value() ? mealPlus.value() : "null"), ",", "\\,");
    return result;
}

#include <regex>

Reponse Reponse::fromCSV(string csv) {
    std::vector<std::string> fields;
    std::string field;
    bool escape = false;

    //splitting the string by comma taking care of the escaped commas
    for (auto c: csv) {
        if (c == '\\' && !escape) {
            escape = true;
            continue;
        }

        if (c == ',' && !escape) {
            fields.push_back(field);
            field.clear();
        } else {
            field += c;
            escape = false;
        }
    }
    fields.push_back(field); // push the last field

    // Check that the csv has the right number of columns
    if (fields.size() != 11) {
        throw invalid_argument("Invalid CSV format");
    }

    auto date = stoll(fields[0]);
    auto noteMood = (fields[1] == "null") ? nullopt : optional<int>(stoi(fields[1]));
    auto adjective = (fields[2] == "null") ? nullopt : optional<string>(fields[2]);
    auto rant = (fields[3] == "null") ? nullopt : optional<string>(fields[3]);
    auto sleepBegin = (fields[4] == "null") ? nullopt : optional<time_t>(stoi(fields[4]));
    auto sleepEnd = (fields[5] == "null") ? nullopt : optional<time_t>(stoi(fields[5]));
    auto sleepQuality = (fields[6] == "null") ? nullopt : optional<int>(stoi(fields[6]));
    auto sleepPlus = (fields[7] == "null") ? nullopt : optional<string>(fields[7]);
    auto mealHour = (fields[8] == "null") ? nullopt : optional<time_t>(stoi(fields[8]));
    auto mealQuality = (fields[9] == "null") ? nullopt : optional<int>(stoi(fields[9]));
    auto mealPlus = (fields[10] == "null") ? nullopt : optional<string>(fields[10]);

    return {(time_t) date, noteMood, adjective, rant, sleepBegin, sleepEnd, sleepQuality, sleepPlus,
            mealHour, mealQuality, mealPlus};
}

ostream &operator<<(ostream &os, const Reponse &reponse) {
    os << "date: " << dateToString(reponse.date) << " noteMood: " << reponse.noteMood.value_or(-1) << " adjective: "
       << reponse.adjective.value_or("null")
       << " rant: " << reponse.rant.value_or("null") << " sleepBegin: "
       << dateToString(reponse.sleepBegin.value_or(0))
       << " sleepEnd: "
       << dateToString(reponse.sleepEnd.value_or(0))
       << " sleepQuality: " << reponse.sleepQuality.value_or(-1)
       << " sleepPlus: " << reponse.sleepPlus.value_or("null")
       << " mealHour: " << dateToString(reponse.mealHour.value_or(0))
       << " mealQuality: "
       << reponse.mealQuality.value_or(-1) << " mealPlus: "
       << reponse.mealPlus.value_or("null");
    return os;
}

void Reponse::writeToFile(const string &filename) const {
    fstream file;
    file.open(filename, ios::out); // Open for reading and writing
    if (!file.is_open()) {
        cerr << "Could not open file" << endl;
        throw invalid_argument("Could not open file");
    }
    file << toCSV() << endl;
    file.close();
}

bool Reponse::operator==(const Reponse &rhs) const {
    return date == rhs.date &&
           noteMood == rhs.noteMood &&
           adjective == rhs.adjective &&
           rant == rhs.rant &&
           sleepBegin == rhs.sleepBegin &&
           sleepEnd == rhs.sleepEnd &&
           sleepQuality == rhs.sleepQuality &&
           sleepPlus == rhs.sleepPlus &&
           mealHour == rhs.mealHour &&
           mealQuality == rhs.mealQuality &&
           mealPlus == rhs.mealPlus &&
           saved == rhs.saved;
}

bool Reponse::operator!=(const Reponse &rhs) const {
    return !(rhs == *this);
}
