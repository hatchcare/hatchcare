/**
 * @file ReponseList.cpp
 * @brief Implementation file for the ReponseList class.
 */

#include "ReponseList.h"

ReponseList::ReponseList(vector<Reponse> reponses) : responses(std::move(reponses)) {}

ReponseList::ReponseList() : responses(vector<Reponse>()) {}

ReponseList::~ReponseList() = default;

void ReponseList::addReponse(const Reponse &reponse) {
    responses.push_back(reponse);
}

Reponse ReponseList::getReponse(int index) const {
    return responses[index];
}

void ReponseList::setReponse(int index, const Reponse &reponse) {
    responses[index] = reponse;
}

void ReponseList::removeReponse(int index) {
    responses.erase(responses.begin() + index);
}

string ReponseList::toCSV() const {
    string result = "date,noteMood,adjective,rant,sleepBegin,sleepEnd,sleepQuality,sleepPlus,mealHour,mealQuality,mealPlus\n";
    for (auto &reponse: responses) {
        result += reponse.toCSV() + "\n";
    }
    return result;
}

ReponseList ReponseList::fromCSV(string csv) {
    vector<Reponse> responses;
    csv.erase(0, csv.find('\n') + 1);
    while (!csv.empty()) {
        responses.push_back(Reponse::fromCSV(csv.substr(0, csv.find('\n'))));
        csv.erase(0, csv.find('\n') + 1);
    }
    return ReponseList(responses);
}

size_t ReponseList::size() const {
    return responses.size();
}

ostream &operator<<(ostream &os, const ReponseList &list) {
    for (auto &reponse: list.responses) {
        os << reponse << endl;
    }
    return os;
}

bool ReponseList::saveToFile(const string &filename) const {
    fstream file;
    file.open(filename, ios::out);
    if (!file.is_open()) {
        cerr << "Error opening file" << filename << endl;
        return false;
    }
    file << toCSV();
    file.close();
    return true;
}

ReponseList ReponseList::fromFile(const string &filename) {
    fstream file;
    file.open(filename, ios::in);
    if (!file.is_open()) {
        throw invalid_argument("File not found");
    }
    ReponseList result = fromCSV(string((istreambuf_iterator<char>(file)), istreambuf_iterator<char>()));
    file.close();
    return result;
}

// This function is used to find correlations between different types of questions.
vector<Correlation> ReponseList::findCorrelation() const {
    // This vector will hold the correlations found.
    vector<Correlation> correlations;

    // These counters are used to ensure that the same question type is not compared twice.
    int countI = 0;

    // Loop through all question types.
    for (auto i: QuestionType::values) {
        int countJ = 0;
        for (auto j: QuestionType::values) {
            // This condition ensures that we don't compare the same question type twice.
            if (countI < countJ) {
                // This condition filters out question types like free texts and other non-analysable question types.
                if (QuestionType::isAnalysable(i) && QuestionType::isAnalysable(j)) {
                    // Convert question types to response types.
                    auto typeI = QuestionType::toReponseType(i);
                    auto typeJ = QuestionType::toReponseType(j);

                    // This vector will be used to split the data into subcategories.
                    // For example, for adjectives, we will split the data for each category.
                    vector<int> subSpliting = {(int) responses.size()};
                    optional<vector<string>> ajdSubList = nullopt;

                    // If the response type is an adjective, split the series of adjectives.
                    if (typeI == ReponseType::adjective) {
                        auto result = splitAdjectiveSeries(i);
                        subSpliting = result.first;
                        ajdSubList = result.second;
                    }
                    if (typeJ == ReponseType::adjective) {
                        auto result = splitAdjectiveSeries(j);
                        subSpliting = result.first;
                        ajdSubList = result.second;
                    }

                    // Construct raw data.
                    vector<optional<reponseTypes>> rawDataI = getVariantList(i);
                    vector<optional<reponseTypes>> rawDataJ = getVariantList(j);

                    vector<vector<int>> dataI = vector<vector<int>>(subSpliting.size());
                    vector<vector<int>> dataJ = vector<vector<int>>(subSpliting.size());

                    int start = 0;
                    int count = 0;
                    for (auto k: subSpliting) {
                        vector<int> subDataI;
                        vector<int> subDataJ;
                        for (int l = start; l < k; ++l) {
                            // If the raw data has a value, convert it to an integer.
                            if (rawDataI[l].has_value() && rawDataJ[l].has_value()) {
                                subDataI.push_back(QuestionType::toInt(i, rawDataI[l].value()));
                                subDataJ.push_back(QuestionType::toInt(j, rawDataJ[l].value()));
                            }
                        }
                        // Assign the subdata to the main data vectors.
                        dataI.at(count) = subDataI;
                        dataJ.at(count) = subDataJ;
                        start = k;
                        count++;
                    }

                    // Now that we have the data in integer form, we just have to apply Spearson correlation for each of them.
                    for (int k = 0; k < subSpliting.size(); ++k) {
                        auto adjectiveCategory = (ajdSubList.has_value() ? optional(ajdSubList.value().at(k))
                                                                         : nullopt);
                        auto correlationValues = AnalysisUtility::findBestCorrelation(dataI.at(k), dataJ.at(k));
                        if (correlationValues.has_value()) {
                            auto correlation = Correlation(i, j, adjectiveCategory,
                                                           correlationValues.value().first,
                                                           correlationValues.value().second);
                            correlations.push_back(correlation);
                        }
                    }
                }
            }
            countJ++;
        }
        countI++;
    }

    // Return the correlations found.
    return correlations;
}

// This function is used to split a series of adjectives into subcategories.
pair<vector<int>, vector<string>> ReponseList::splitAdjectiveSeries(const QuestionType::Type &questionType) const {
    // Get the list of strings for the given question type.
    auto data = getStringList(questionType);

    // These vectors will hold the results and the adjective categories.
    vector<int> result;
    vector<string> adjectiveCategories;

    // This optional string will hold the previous value in the loop.
    // It is initialized to nullopt for the first iteration.
    optional<string> previous = nullopt;

    // This counter is used to keep track of the number of iterations.
    int count = 0;

    // Loop through all the data.
    for (auto &i: data) {
        // This string will hold the main category of the current iteration.
        string value;

        // If the current data has a value, find its main category.
        if (i.has_value()) {
            // Loop through all the sublists of adjectives.
            for (const auto &pair: adjectivesSubList) {
                // If the current value is found in the sublist, set the main category to the first value of the pair.
                auto iterator = find(pair.second.begin(), pair.second.end(), i.value());
                if (iterator != pair.second.end()) {
                    value = pair.first;
                    break;
                }
            }
        }

        // If the previous value has a value and it is different from the current main category,
        // add the count to the result vector and the previous value to the adjective categories vector.
        // Then reset the count and set the previous value to the current main category.
        if (previous.has_value() && value != previous) {
            result.push_back(count);
            adjectiveCategories.push_back(value);
            previous = value;
        }
            // If the previous value does not have a value, we are on the first iteration,
            // so we need to set the initial previous value to the current main category.
        else if (!previous.has_value()) {
            previous = value;
            adjectiveCategories.push_back(previous.value());
        }
        count++;
    }
    result.push_back(count);

    // Return the result vector and the adjective categories vector.
    return {result, adjectiveCategories};
}

vector<optional<int>> ReponseList::getIntList(QuestionType::Type type) const {
    vector<optional<int>> result;
    for (auto &reponse: getVariantList(type)) {
        if (reponse.has_value()) {
            if (holds_alternative<int>(reponse.value())) {
                result.emplace_back(get<int>(reponse.value()));
            } else if (QuestionType::isAnalysable(type)) {
                result.emplace_back(QuestionType::toInt(type, reponse.value()));
            } else {
                result.emplace_back(nullopt);
            }
        } else {
            result.emplace_back(nullopt);
        }
    }
    return result;
}

vector<optional<string>> ReponseList::getStringList(QuestionType::Type type) const {
    vector<optional<string>> result;
    for (auto &reponse: getVariantList(type)) {
        if (reponse.has_value() && holds_alternative<string>(reponse.value())) {
            result.emplace_back(get<string>(reponse.value()));
        } else {
            result.emplace_back(nullopt);
        }
    }
    return result;
}

[[maybe_unused]] vector<optional<time_t>> ReponseList::getTimeList(QuestionType::Type type) const {
    // use the variant and just type it
    vector<optional<time_t>> result;
    for (auto &reponse: getVariantList(type)) {
        if (reponse.has_value() && holds_alternative<time_t>(reponse.value())) {
            result.emplace_back(get<time_t>(reponse.value()));
        } else {
            result.emplace_back(nullopt);
        }
    }
    return result;
}

vector<long> ReponseList::getDateList() const {
    vector<long> result;
    for (auto &reponse: responses) {
        result.push_back(reponse.date);
    }
    return result;
}

vector<optional<reponseTypes>> ReponseList::getVariantList(QuestionType::Type type) const {
    vector<optional<reponseTypes>> result;
    for (auto &reponse: responses) {
        switch (type) {
            case QuestionType::noteMood:
                result.emplace_back(reponse.noteMood);
                break;
            case QuestionType::adjective:
                result.emplace_back(reponse.adjective);
                break;
            case QuestionType::rant:
                result.emplace_back(reponse.rant);
                break;
            case QuestionType::sleepBegin:
                result.emplace_back(reponse.sleepBegin);
                break;
            case QuestionType::sleepEnd:
                result.emplace_back(reponse.sleepEnd);
                break;
            case QuestionType::sleepQuality:
                result.emplace_back(reponse.sleepQuality);
                break;
            case QuestionType::sleepPlus:
                result.emplace_back(reponse.sleepPlus);
                break;
            case QuestionType::mealHour:
                result.emplace_back(reponse.mealHour);
                break;
            case QuestionType::mealQuality:
                result.emplace_back(reponse.mealQuality);
                break;
            case QuestionType::mealPlus:
                result.emplace_back(reponse.mealPlus);
                break;
            default:
                result.emplace_back(nullopt);
        }
    }
    return result;
}

bool ReponseList::operator==(const ReponseList &rhs) const {
    for (int i = 0; i < rhs.size(); ++i) {
        if (responses[i] != rhs.responses[i]) {
            return false;
        }
    }
    return true;
}

bool ReponseList::operator!=(const ReponseList &rhs) const {
    return !(rhs == *this);
}


