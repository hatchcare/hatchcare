/**
 * @file Correlation.h
 * @brief This file contains the declaration of the Correlation class, which represents a correlation between two question types.
 */

#ifndef HATCHCARE_CORRELATION_H
#define HATCHCARE_CORRELATION_H

#include <ostream>
#include "../question/Question.h"

/**
 * Represents a correlation between two question types.
 */
class Correlation {
public:
    /**
     * Constructs a Correlation object.
     *
     * @param questionTypeA The type of the first question.
     * @param questionTypeB The type of the second question.
     * @param adjectiveCategory The type of adjective if the correlation is of type adjective.
     * @param coefficient The coefficient of the spearman correlation.
     * @param lag The lag of the correlation.
     */
    Correlation(QuestionType::Type questionTypeA, QuestionType::Type questionTypeB,
                const optional<string> &adjectiveCategory, double coefficient, int lag);

    /**
     * Overloads the << operator to output the Correlation object to an ostream.
     *
     * @param os The output stream.
     * @param correlation The Correlation object to be output.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const Correlation &correlation);

    QuestionType::Type questionTypeA;   // The type of the first question
    QuestionType::Type questionTypeB;   // The type of the second question
    optional<string> adjectiveCategory; // The type of adjective if the correlation is of type adjective
    double coefficient;                 // The coefficient of the spearman correlation
    int lag;                            // The lag of the correlation
};

#endif // HATCHCARE_CORRELATION_H
