/**
 * @file Graph.h
 * @brief Header file for the Graph class.
 */

#ifndef HATCHCARE_GRAPH_H
#define HATCHCARE_GRAPH_H

#include <tuple>
#include <vector>
#include <ostream>
#include <cmath>
#include "Correlation.h"

using namespace std;

/**
 * @class Graph
 * @brief Represents a graph with a set of points.
 *
 * The Graph class provides methods to manipulate a set of points in a graph.
 * It allows adding, getting, and removing points from the graph.
 * The class also provides methods to convert the graph to CSV format and to create a graph from a CSV string.
 * The size of the graph can be obtained using the size() method.
 * The class overloads the equality and inequality operators for comparison.
 */
class Graph {
public:
    /**
     * @brief Constructs a Graph object with the given list of points.
     * @param data The list of points.
     */
    Graph(const vector<vector<pair<float, float>>> &data, Correlation correlation);

    /**
     * @brief displays the graph
     * the graph should be sized according to the correlation to visualize the correlation
     */
    friend ostream &operator<<(ostream &os, const Graph &graph);

private:
    /**
     * @brief The list of points in the graph.
     * the data are stored as a vector of curves
     * each curve is a vector of pairs
     * the first one is the x value and the second one is the y value
    */
    vector<vector<pair<float, float>>> data;
    /**
     * @brief The correlation of the graph.
     */
    Correlation correlation;
};

#endif // HATCHCARE_GRAPH_H
