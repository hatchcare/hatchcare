#ifndef HATCHCARE_REPONSETYPE_H
#define HATCHCARE_REPONSETYPE_H

#include <variant>
#include <string>

using namespace std;

class ReponseType {
public:
    /**
     * @brief The enumeration of possible response types for a question.
     */
    enum Type {
        text,      ///< Text response type
        number,    ///< Numeric response type
        date,      ///< Date response type
        adjective, ///< Adjective response type
    };

    static constexpr Type values[] = {text, number, date, adjective};
};

/**
 * @brief The possible types of responses.
 *
 * This typedef represents a variant type that can hold values of different types, including string, time_t, int, and bool.
 * It is used to define the possible types of responses in the application.
 */
typedef variant<string, time_t, int, bool> reponseTypes;

#endif // HATCHCARE_REPONSETYPE_H
