#include "Correlation.h"


Correlation::Correlation(QuestionType::Type questionTypeA, QuestionType::Type questionTypeB,
                         const optional<string> &adjectiveCategory, double coefficient, int lag) : questionTypeA(
        questionTypeA), questionTypeB(questionTypeB), adjectiveCategory(adjectiveCategory), coefficient(coefficient),
                                                                                                   lag(lag) {}

ostream &operator<<(ostream &os, const Correlation &correlation) {
    os << "questionTypeA: " << correlation.questionTypeA << " questionTypeB: " << correlation.questionTypeB
       << " adjectiveCategory: " << correlation.adjectiveCategory.value_or("") << " coefficient: "
       << correlation.coefficient
       << " lag: " << correlation.lag;
    return os;
}
