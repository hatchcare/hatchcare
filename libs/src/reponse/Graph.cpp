/**
 * @file Graph.cpp
 * @brief Implementation file for the Graph class.
 */

#include "Graph.h"

#include <utility>

Graph::Graph(const vector<vector<pair<float, float>>> &data, Correlation correlation) : data(data),
                                                                                        correlation(std::move(
                                                                                                correlation)) {}

ostream &operator<<(ostream &os, const Graph &graph) {
    // Determine the console height
    int graphHeight = 15; // You may need to adjust this value
    int graphWidth = 25; // You may need to adjust this value

    vector<vector<char>> displayData = vector<vector<char>>(graphHeight, vector<char>(graphWidth, ' '));

    // Find the maximum value in the data
    float maxVertical = 0;
    float minVertical = 0;
    float maxHorizontal = 0;
    float minHorizontal = 0;
    for (const auto &vec: graph.data) {
        for (const auto &pair: vec) {
            maxVertical = max(maxVertical, pair.second);
            minVertical = min(minVertical, pair.second);
            maxHorizontal = max(maxHorizontal, pair.first);
            minHorizontal = min(minHorizontal, pair.first);
        }
    }
    float height = maxVertical - minVertical;
    float width = maxHorizontal - minHorizontal;

    float verticalScale = (float) graphHeight / height;
    float horizontalScale = (float) graphWidth / width;

    minHorizontal = minHorizontal * horizontalScale;
    minVertical = minVertical * verticalScale;

    float verticalDrift = -minVertical;
    float horizontalDrift = minHorizontal;

    //draw the axis according to the max and min
    for (int i = 0; i < graphHeight; i++) {
        displayData[i][(int) ((float) graphWidth / 2 + horizontalDrift)] = '|';
    }
    for (int i = 0; i < graphWidth; i++) {
        displayData[(int) ((float) graphHeight / 2 + verticalDrift)][i] = '-';
    }


    //place the points on the graph
    for (const auto &vec: graph.data) {
        for (const auto &pair: vec) {
            int x = (int) (pair.first * horizontalScale);
            int y = graphHeight - (int) (pair.second * verticalScale);

            if (x >= 0 && x < graphWidth && y >= 0 && y < graphHeight) {
                displayData[y][x] = '*';
            }
        }
    }

    // Print the graph
    for (int i = 0; i < graphHeight; i++) {
        for (int j = 0; j < graphWidth; j++) {
            os << displayData[i][j];
        }
        os << endl;
    }

    return os;
}