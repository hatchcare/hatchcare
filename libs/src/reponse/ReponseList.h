/**
 * @file ReponseList.h
 * @brief Header file for the ReponseList class.
 */

#ifndef HATCHCARE_REPONSELIST_H
#define HATCHCARE_REPONSELIST_H

#include <vector>
#include <ostream>
#include <fstream>
#include <cmath>
#include "../question/QuestionAdjective.h"
#include "AnalysisUtility.hpp"
#include "Reponse.h"
#include "Correlation.h"
#include "../question/QuestionType.h"

/**
 * @class ReponseList
 * @brief Represents a list of responses.
 *
 * The ReponseList class provides methods to manipulate a list of Reponse objects.
 * It allows adding, getting, setting, and removing responses from the list.
 * The class also provides methods to convert the list to CSV format and to create a list from a CSV string.
 * The size of the list can be obtained using the size() method.
 * The class overloads the equality and inequality operators for comparison.
 */
class ReponseList {
public:
    /**
     * @brief Constructs a ReponseList object with the given list of responses.
     * @param reponses The list of responses.
     */
    explicit ReponseList(vector<Reponse> reponses);

    /**
     * @brief Default constructor for the ReponseList object.
     */
    ReponseList();

    /**
     * @brief Destructor for the ReponseList object.
     */
    virtual ~ReponseList();

    /**
     * @brief Adds a response to the list.
     * @param reponse The response to add.
     */
    void addReponse(const Reponse &reponse);

    /**
     * @brief Gets the response at the specified index.
     * @param index The index of the response to get.
     * @return The response at the specified index.
     */
    [[nodiscard]] Reponse getReponse(int index) const;

    /**
     * @brief Sets the response at the specified index.
     * @param index The index of the response to set.
     * @param reponse The new response.
     */
    void setReponse(int index, const Reponse &reponse);

    /**
     * @brief Removes the response at the specified index.
     * @param index The index of the response to remove.
     */
    void removeReponse(int index);

    /**
     * @brief Converts the list to a CSV string.
     * @return The CSV representation of the list.
     */
    [[nodiscard]] string toCSV() const;

    /**
     * @brief Saves the list to a file.
     * @param filename The name of the file to save to.
     * @return True if the list was saved successfully, false otherwise.
     */
    [[nodiscard]] bool saveToFile(const string &filename) const;

    /**
     * @brief Creates a ReponseList object from a CSV string.
     * @param csv The CSV string representing the list.
     * @return The ReponseList object created from the CSV string.
     */
    static ReponseList fromCSV(string csv);

    /**
     * @brief Creates a ReponseList object from a file.
     * @param filename The name of the file to read.
     * @return The ReponseList object created from the file.
     */
    static ReponseList fromFile(const string &filename);

    /**
     * @brief Gets the size of the list.
     * @return The size of the list.
     */
    [[nodiscard]] size_t size() const;

    /**
     * @brief Try to find correlation between responses.
     * @return A vector of Correlation objects representing the correlations found.
     */
    [[nodiscard]] vector<Correlation> findCorrelation() const;

    /**
     * @brief Gets a list of optional integers based on the specified question type.
     * @param type The question type.
     * @return A vector of optional integers.
     */
    [[nodiscard]] vector<optional<int>> getIntList(QuestionType::Type type) const;

    /**
     * @brief Gets a list of optional strings based on the specified question type.
     * @param type The question type.
     * @return A vector of optional strings.
     */
    [[nodiscard]] vector<optional<string>> getStringList(QuestionType::Type type) const;

    /**
     * @brief Gets a list of optional time_t objects based on the specified question type.
     * @param type The question type.
     * @return A vector of optional time_t objects.
     */
    [[maybe_unused]] [[nodiscard]] vector<optional<time_t>> getTimeList(QuestionType::Type type) const;

    /**
     * @brief Gets a list of optional reponseTypes based on the specified question type.
     * @param type The question type.
     * @return A vector of optional reponseTypes.
     */
    [[nodiscard]] vector<optional<reponseTypes>> getVariantList(QuestionType::Type type) const;

    [[nodiscard]] vector<long> getDateList() const;

    /**
     * @brief Splits the adjective series based on the adjective main category.
     * @param questionType The question type.
     * @return A pair of vectors, where the first vector contains integers and the second vector contains strings.
     */
    [[nodiscard]] pair<vector<int>, vector<string>> splitAdjectiveSeries(const QuestionType::Type &questionType) const;

    /**
     * @brief Overloads the stream insertion operator for printing the ReponseList object.
     * @param os The output stream.
     * @param list The ReponseList object to print.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const ReponseList &list);

    /**
     * @brief Overloaded equality operator for comparing two ReponseList objects.
     *
     * This operator compares the current ReponseList object with the specified `rhs` object
     * and returns true if they are equal, and false otherwise.
     *
     * @param rhs The ReponseList object to compare with.
     * @return True if the objects are equal, false otherwise.
     */
    bool operator==(const ReponseList &rhs) const;

    /**
     * @brief Overloaded inequality operator for comparing two ReponseList objects.
     *
     * This operator compares the current ReponseList object with the specified `rhs` object
     * and returns `true` if they are not equal, and `false` otherwise.
     *
     * @param rhs The ReponseList object to compare with.
     * @return `true` if the objects are not equal, `false` otherwise.
     */
    bool operator!=(const ReponseList &rhs) const;

private:
    vector<Reponse> responses; ///< The list of responses.
};

#endif // HATCHCARE_REPONSELIST_H
