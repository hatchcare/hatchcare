/**
 * @file QuestionNumber.cpp
 * @brief Implementation file for the QuestionNumber class.
 */

#include "QuestionNumber.h"

QuestionNumber::QuestionNumber(const string &id, QuestionType::Type questionType, const string &questionText,
                               const map<int, pair<shared_ptr<Question>, AnimationTriggers::Type>> &reponseMap, int min,
                               int max,
                               int step) : Question(
        id, questionType, questionText), min(min), max(max), step(step), reponseMap(reponseMap) {
    assert(min < max);
    assert(step > 0);
    assert(questionType == QuestionType::noteMood || questionType == QuestionType::sleepQuality ||
           questionType == QuestionType::mealQuality);
}

QuestionNumber::QuestionNumber() {
    min = 0;
    max = 0;
    step = 0;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> QuestionNumber::askConsole(Reponse *reponse) const {
    displayText(questionText + "\n");
    int value;
    do {
        for (int i = min; i < max; i += step) {
            displayText(to_string(i) + "\n");
        }
        displayText(
                "Enter a value between " + to_string(min) + " and " + to_string(max) + " with a step of " +
                to_string(step) + "\n");
        cout << ANSWER_ARROW;
        cin >> value;
    } while ((value < min || value > max || (value - min) % step != 0));

    return saveReponse(reponse, value);
}

vector<string> QuestionNumber::getResponsePossible() const {
    vector<string> result;
    for (int i = min; i < max; i += step) {
        result.push_back(to_string(i));
    }
    return result;
}

vector<shared_ptr<Question>> QuestionNumber::getPossibleNextQuestions() const {
    vector<shared_ptr<Question>> result;
    for (auto &i: reponseMap) {
        result.push_back(i.second.first);
    }
    return result;
}

ostream &operator<<(ostream &os, const QuestionNumber &number) {
    os << static_cast<const Question &>(number) << " min: " << number.min << " max: " << number.max << " step: "
       << number.step << " reponseMap: ";
    for (auto &i: number.reponseMap) {
        os << i.first << " : " << i.second.first << " ";
    }
    return os;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
QuestionNumber::saveReponse(Reponse *reponse, reponseTypes response) const {
    switch (questionType) {
        case QuestionType::noteMood:
            reponse->noteMood = get<int>(response);
            break;
        case QuestionType::sleepQuality:
            reponse->sleepQuality = get<int>(response);
            break;
        case QuestionType::mealQuality:
            reponse->mealQuality = get<int>(response);
            break;
        default:
            throw invalid_argument("Invalid question type");
    }
    auto nextQuestion = reponseMap.find(get<int>(response));
    if (nextQuestion == reponseMap.end()) {
        return nullopt;
    }
    return nextQuestion->second;
}
