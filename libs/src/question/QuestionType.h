#ifndef HATCHCARE_QUESTIONTYPE_H
#define HATCHCARE_QUESTIONTYPE_H

#include <string>
#include <variant>
#include "../reponse/ReponseType.h"

using namespace std;

/**
 * @enum QuestionType
 * @brief Enumeration representing the types of questions.
 */
class QuestionType
{
public:
    enum Type
    {
        noteMood,     ///< Mood rating question
        adjective,    ///< Adjective question
        rant,         ///< Rant question
        sleepBegin,   ///< Sleep start time question
        sleepEnd,     ///< Sleep end time question
        sleepQuality, ///< Sleep quality rating question
        sleepPlus,    ///< Additional sleep details question
        mealHour,     ///< Meal time question
        mealQuality,  ///< Meal quality rating question
        mealPlus,     ///< Additional meal details question
    };
    // define value to be able to iterate over the enum
    static constexpr Type values[] = {noteMood, adjective, rant, sleepBegin, sleepEnd, sleepQuality,
                                      sleepPlus, mealHour, mealQuality, mealPlus};

    /**
     * @brief Convert a QuestionType to a string.
     * @param type
     * @return string
     * Be careful to keep this function in sync with the enum.
     * Be also careful because the string is used in c wrapper
     */
    static string to_string(Type type);

    /**
     * @brief Converts a question type to a response type.
     *
     * This function takes a question type and converts it to the corresponding response type.
     *
     * @param type The question type to convert.
     * @return The corresponding response type.
     */
    static ReponseType::Type toReponseType(Type type);

    /**
     * Checks if the given question type is analysable.
     *
     * @param type The question type to check.
     * @return True if the question type is analysable, false otherwise.
     */
    static bool isAnalysable(Type type);

    /**
     * Converts the given `value` of type `reponseTypes` to an integer based on the specified `type`.
     *
     * @param type The type of the question.
     * @param value The value to be converted.
     * @return The converted integer value.
     */
    static int toInt(Type type, reponseTypes value);
};

#endif // HATCHCARE_QUESTIONTYPE_H
