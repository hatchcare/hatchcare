/**
 * @file QuestionAdjective.h
 * @brief This file contains the declaration of the QuestionAdjective class, which represents a question with adjective type.
 *
 * The QuestionAdjective class inherits from the Question class and provides functionality to handle adjective questions.
 * It includes methods to ask the question in the console, get the response type, get possible response options, and get possible next questions.
 * The class also defines a map of adjective sublists for each adjective type.
 */

#ifndef HATCHCARE_QUESTIONADJECTIVE_H
#define HATCHCARE_QUESTIONADJECTIVE_H

#define ADJECTIVES_SIZE 6

#include <ostream>
#include <variant>
#include <array>
#include <cassert>
#include "Question.h"
#include "QuestionType.h"
#include "../common.h"

/**
 * @brief Represents a question of type "adjective".
 *
 * This class inherits from the base class Question and provides additional functionality specific to adjective questions.
 */
class QuestionAdjective : public Question {
public:
    /**
     * @brief Constructs a QuestionAdjective object with the specified parameters.
     *
     * @param id The ID of the question.
     * @param questionType The type of the question.
     * @param questionText The text of the question.
     * @param reponseMap A map of possible responses to the question.
     */
    QuestionAdjective(const string &id, QuestionType::Type questionType, const string &questionText,
                      const map<string, pair<shared_ptr<Question>, AnimationTriggers::Type>> &reponseMap);

    /**
     * @brief Default constructor for QuestionAdjective.
     */
    QuestionAdjective();

    /**
     * @brief Asks the question in the console and returns the next question based on the user's response.
     *
     * @param reponse A pointer to the Reponse object.
     * @return An optional shared pointer to the next question.
     */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    askConsole(Reponse *reponse) const override;

    /**
     * @brief Gets the response type of the question.
     *
     * @return The response type as a string.
     */
    [[nodiscard]] string getReponseType() const override {
        return "adjective";
    }

    /**
     * @brief Gets the possible responses to the question.
     *
     * @return A vector of strings representing the possible responses.
     */
    [[nodiscard]] vector<string> getResponsePossible() const override;

    /**
     * @brief Gets the possible next questions based on the user's response.
     *
     * @return A vector of shared pointers to the possible next questions.
     */
    [[nodiscard]] vector<shared_ptr<Question>> getPossibleNextQuestions() const override;

    /**
     * @brief Save the reponse into the reponse object
     * @param reponse The response object associated with the question.
     * @param response The user's response to the question.
     * @return The next question to ask
     */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    saveReponse(Reponse *reponse, reponseTypes response) const override;

    /**
     * @brief Overloads the << operator to output the QuestionAdjective object to an output stream.
     *
     * @param os The output stream.
     * @param questionAdjective The QuestionAdjective object to be output.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const QuestionAdjective &questionAdjective);

private:
    /**
     * @brief A map of possible responses to the question.
     * The map has string keys representing the possible responses and pair values representing the next question and animation trigger type.
     * It can contain a precise adjective but also a main category adjective.
     * The precise adjective will be used first and if not found, the main category will be used.
     */
    map<string, pair<shared_ptr<Question>, AnimationTriggers::Type>> reponseMap;
};

/**
 * @brief List of adjectives.
 *
 * This array contains a list of adjectives that can be used in questions.
 * The adjectives are stored as strings.
 */
const array<string, ADJECTIVES_SIZE> adjectivesList = {
        "content",
        "confiant",
        "triste",
        "en colère",
        "paniqué",
        "fatigué"};

/**
 * @brief A map that stores a list of adjectives categorized by their corresponding content.
 *
 * The map has string keys representing the content and vector<string> values representing the adjectives.
 * Each content is associated with a list of adjectives that describe it.
 * The order of the adjectives in the list determines their power or intensity.
 *
 * Example usage:
 * const map<string, vector<string>> adjectivesSubList = {
 *     {"content", {"adjective1", "adjective2", "adjective3"}},
 *     {"content2", {"adjective4", "adjective5"}}
 * };
 */
const map<string, vector<string>> adjectivesSubList = {
        // HOYE HOYE
        // si vous changez cette liste, pensez à update questionDatas.h pour match !!
        // ATTENTION L'ORDRE CHANGE LA VALEUR DE L'ADJECTIF (ORDONNÉE PAR PUISSANCE)
        {"content",   {"détendu",    "motivé",    "heureux",   "joyeux",       "enthousiaste", "énergique"}},
        {"confiant",  {"performant", "beau",      "concentré", "épanoui",      "fier"}},
        {"triste",    {"déprimé",    "dépressif", "abattu",    "mélancolique", "coupable",     "seul"}},
        {"en colère", {"furieux",    "tendu",     "agité",     "exaspéré",     "enragé"}},
        {"paniqué",   {"peureux",    "nerveux",   "phobique",  "inquiet",      "angoissé"}},
        {"fatigué",   {"épuisé",     "somnolant", "engourdi",  "usé",          "ennuyé"}}};

#endif // HATCHCARE_QUESTIONADJECTIVE_H
