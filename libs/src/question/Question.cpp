/**
 * @file Question.cpp
 * @brief Implementation file for the Question class.
 */

#include <chrono>
#include <utility>
#include "Question.h"

Question::Question(string id, QuestionType::Type questionType, string questionText) : id(std::move(id)),
                                                                                      questionType(questionType),
                                                                                      questionText(std::move(
                                                                                              questionText)) {}


#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-no-recursion"

string Question::toMermaid(unordered_set<string> &questionsDone) const {
    string result;
    result += "class " + id + " {\n";
    result += "questionType : " + getQuestionType() + "\n";
    result += "question : " + questionText + "\n";
    result += "responsePossible : [";
    for (int i = 0; i < getResponsePossible().size(); i++) {
        result += getResponsePossible()[i];
        if (i < getResponsePossible().size() - 1) {
            result += ", ";
        }
    }
    result += "]\n";
    result += "ReponseType:Type : " + getReponseType() + "\n";
    result += "questionType : " + getQuestionType() + "\n";
    result += "}\n";
    for (auto &i: getPossibleNextQuestions()) {
        result += i->id + "<|--" + id + "\n";
        if (questionsDone.find(i->id) !=
            questionsDone.end()) {
            continue;
        }
        questionsDone.insert(i->id);
        result += i->toMermaid(questionsDone);
    }
    return result;
}

#pragma clang diagnostic pop

[[nodiscard]] string Question::toMermaid() const {
    string result = "classDiagram\n";
    unordered_set<string> questionsDone;
    questionsDone.insert(id);
    return result + toMermaid(questionsDone);
}

[[nodiscard]] string Question::getQuestionType() const {
    switch (questionType) {
        case QuestionType::noteMood:
            return "noteMood";
        case QuestionType::adjective:
            return "adjective";
        case QuestionType::rant:
            return "rant";
        case QuestionType::sleepBegin:
            return "sleepBegin";
        case QuestionType::sleepEnd:
            return "sleepEnd";
        case QuestionType::sleepQuality:
            return "sleepQuality";
        case QuestionType::sleepPlus:
            return "sleepPlus";
        case QuestionType::mealHour:
            return "mealHour";
        case QuestionType::mealQuality:
            return "mealQuality";
        case QuestionType::mealPlus:
            return "mealPlus";
        default:
            throw invalid_argument("Invalid question type");
    }
}

Question::Question() {
    id = "";
    questionType = QuestionType::noteMood;
    questionText = "";
}

ostream &operator<<(ostream &os, const Question &question) {
    os << "id: " << question.id << " questionType: " << question.questionType << " questionText: "
       << question.questionText;
    return os;
}

Question::~Question() = default;
