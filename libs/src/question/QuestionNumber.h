/**
 * @file QuestionNumber.h
 * @brief Header file for the QuestionNumber class.
 *
 * This file contains the declaration of the QuestionNumber class, which is a subclass of the Question class.
 * The QuestionNumber class represents a question that requires a numerical response.
 * It provides methods for asking the question, getting the possible response options, and retrieving the response type.
 * The class also includes member variables for storing the minimum and maximum values of the response, as well as the step size.
 * Additionally, it contains a map of possible response values and their corresponding next questions.
 */

#ifndef HATCHCARE_QUESTIONNUMBER_H
#define HATCHCARE_QUESTIONNUMBER_H

#include <cassert>
#include <ostream>
#include "Question.h"
#include "../common.h"

/**
 * @brief The QuestionNumber class represents a question that requires a numerical response.
 *
 * This class inherits from the base class Question and provides additional functionality for handling numerical responses.
 */
class QuestionNumber : public Question {
public:
    /**
     * @brief Constructs a QuestionNumber object with the specified parameters.
     *
     * @param id The ID of the question.
     * @param questionType The type of the question.
     * @param questionText The text of the question.
     * @param reponseMap A map of possible responses.
     * @param min The minimum value allowed for the response.
     * @param max The maximum value allowed for the response.
     * @param step The step size for the response.
     */
    QuestionNumber(const string &id, QuestionType::Type questionType, const string &questionText,
                   const map<int, pair<shared_ptr<Question>, AnimationTriggers::Type>> &reponseMap, int min, int max,
                   int step);

    /**
     * @brief Default constructor for QuestionNumber.
     */
    QuestionNumber();

    /**
     * @brief Destructor for QuestionNumber.
     */
    ~QuestionNumber() override = default;

    /**
     * @brief Asks the question in the console and returns the next question based on the response.
     *
     * @param reponse A pointer to the response object.
     * @return An optional shared pointer to the next question.
     */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    askConsole(Reponse *reponse) const override;

    /**
     * @brief Returns a vector of possible response values.
     *
     * @return A vector of strings representing the possible response values.
     */
    [[nodiscard]] vector<string> getResponsePossible() const override;

    /**
     * @brief Returns the type of the response.
     *
     * @return A string representing the type of the response.
     */
    [[nodiscard]] string getReponseType() const override {
        return "number";
    }

    /**
 * @brief Save the reponse into the reponse object
 * @param reponse The response object associated with the question.
 * @param response The user's response to the question.
 * @return The next question to ask
 */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    saveReponse(Reponse *reponse, reponseTypes response) const override;

    /**
     * @brief Returns a vector of possible next questions.
     *
     * @return A vector of shared pointers to possible next questions.
     */
    [[nodiscard]] vector<shared_ptr<Question>> getPossibleNextQuestions() const override;

    /**
     * @brief Overloaded stream insertion operator for QuestionNumber.
     *
     * @param os The output stream.
     * @param number The QuestionNumber object to be inserted into the stream.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const QuestionNumber &number);

    int min;                                   ///< The minimum value allowed for the response.
    int max;                                   ///< The maximum value allowed for the response.
    int step;                                  ///< The step size for the response.

private:

    map<int, pair<shared_ptr<Question>, AnimationTriggers::Type>> reponseMap; ///< A map of possible responses.
};

#endif // HATCHCARE_QUESTIONNUMBER_H
