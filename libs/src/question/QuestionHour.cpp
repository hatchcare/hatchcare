/**
 * @file QuestionDate.cpp
 * @brief Implementation file for the QuestionDate class.
 */

#include <sstream>
#include <iomanip>
#include <valarray>
#include "QuestionHour.h"

QuestionHour::QuestionHour(const string &id, QuestionType::Type questionType, const string &questionText,
                           const map<pair<int, int>, pair<shared_ptr<Question>, AnimationTriggers::Type>> &nextQuestionMap,
                           int precision)
        : Question(id,
                   questionType,
                   questionText),
          nextQuestionMap(
                  nextQuestionMap),
          precision(
                  precision) {
    assert(questionType == QuestionType::sleepBegin ||
           questionType == QuestionType::sleepEnd ||
           questionType == QuestionType::mealHour);
    assert(precision >= 0 && precision <= 2);
}

QuestionHour::QuestionHour() {
    precision = 0;
}

vector<string> QuestionHour::getResponsePossible() const {
    switch (precision) {
        case 0:
            return {"YYYY"};
        case 1:
            return {"YYYY-MM"};
        case 2:
            return {"YYYY-MM-DD"};
        case 3:
            return {"YYYY-MM-DD HH"};
        case 4:
            return {"YYYY-MM-DD HH:MM"};
        case 5:
            return {"YYYY-MM-DD HH:MM:SS"};
        default:
            throw invalid_argument("Invalid precision");
    }
}

vector<shared_ptr<Question>> QuestionHour::getPossibleNextQuestions() const {
    vector<shared_ptr<Question>> result = {};
    for (auto &i: nextQuestionMap) {
        result.push_back(nextQuestionMap.at(i.first).first);
    }
    return result;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> QuestionHour::askConsole(Reponse *reponse) const {
    displayText(questionText + "\n");
    string rawReponse;
    string format;
    string formatDisplayed;
    switch (precision) {
        case 0:
            format = "%H";
            formatDisplayed = "HH";
            break;
        case 1:
            format = "%H:%M";
            formatDisplayed = "HH:MM";
            break;
        case 2:
            format = "%H:%M:%S";
            formatDisplayed = "HH:MM:SS";
            break;
        default:
            throw invalid_argument("Invalid precision");
    }

    tm tm = {};
    istringstream ss;
    do {
        displayText("Veuillez entrer une date au format " + formatDisplayed + "\n");
        cout << ANSWER_ARROW;
        cin >> rawReponse;
        ss = istringstream(rawReponse);
        ss >> get_time(&tm, format.c_str());
        if (ss.fail()) {
            displayText("Parse failed\n");
        }
    } while (ss.fail());
    int time = tm.tm_hour * 3600 + tm.tm_min * 60 + tm.tm_sec;
    cout << time << endl;

    return saveReponse(reponse, time);
}

ostream &operator<<(ostream &os, const QuestionHour &questionDate) {
    os << static_cast<const Question &>(questionDate) << " nextQuestionMap: ";
    for (auto &i: questionDate.nextQuestionMap) {
        os << i.first.first << " to " << i.first.second << " : " << i.second.first << " ";
    }
    os << " precision: " << questionDate.precision;
    return os;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
QuestionHour::saveReponse(Reponse *reponse, reponseTypes response) const {
    switch (questionType) {
        case QuestionType::sleepBegin:
            reponse->sleepBegin = get<int>(response);
            break;
        case QuestionType::sleepEnd:
            reponse->sleepEnd = get<int>(response);
            break;
        case QuestionType::mealHour:
            reponse->mealHour = get<int>(response);
            break;
        default:
            throw invalid_argument("Invalid question type");
    }
    for (auto i: nextQuestionMap) {
        if (get<int>(response) >= i.first.first && get<int>(response) <= i.first.second) {
            return i.second;
        }
    }
    return nullopt;
}

[[maybe_unused]] int QuestionHour::getPrecision() const {
    return precision;
}
