/**
 * @file Question.h
 * @brief The file contains the declaration of the Question class.
 */

#ifndef HATCHCARE_QUESTION_H
#define HATCHCARE_QUESTION_H

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <utility>
#include <unordered_set>
#include <memory>
#include <variant>
#include "../reponse/Reponse.h"
#include "../reponse/ReponseType.h"
#include "QuestionType.h"


using namespace std;


/**
 * @class Question
 * @brief The Question class represents a question with its possible responses and associated actions.
 */
class Question {
public:
    /**
     * @brief Constructs a Question object.
     * @param id The unique identifier of the question.
     * @param questionType The type of the question.
     * @param questionText The text of the question.
     */
    Question(string id, QuestionType::Type questionType, string questionText);

    /**
     * @brief Default constructor for the Question object.
     */
    Question();

    /**
     * @brief Destructor for the Question object.
     */
    virtual ~Question();

    /**
     * @brief Generates the Mermaid diagram representation of the question and its follow-up questions.
     * @param questionsDone The set of questions already processed to avoid infinite loops.
     * @return The Mermaid diagram representation of the question and its follow-up questions.
     */
    string toMermaid(unordered_set<string> &questionsDone) const;

    /**
     * @brief Generates the Mermaid diagram representation of the question.
     * @return The Mermaid diagram representation of the question.
     */
    [[nodiscard]] string toMermaid() const;

    /**
     * @brief Asks the question in the console and returns the user's response.
     * @param reponse The response object associated with the question.
     * @return The next question to ask
     */
    virtual optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> askConsole(Reponse *reponse) const = 0;

    /**
     * @brief Save the reponse into the reponse object
     * @param reponse The response object associated with the question.
     * @param response The user's response to the question.
     * @return The next question to ask
     */
    [[nodiscard]] virtual optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    saveReponse(Reponse *reponse, reponseTypes response) const = 0;

    /**
     * @brief Overloaded stream insertion operator for printing the Question object.
     * @param os The output stream.
     * @param question The Question object to print.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const Question &question);

    /**
     * @brief Gets the type of the question.
     * @return The type of the question.
     */
    [[nodiscard]] string getQuestionType() const;

    /**
     * @brief Gets the type of the response expected for the question.
     * @return The type of the response expected for the question.
     */
    [[nodiscard]] virtual string getReponseType() const = 0;

    /**
     * @brief Gets the possible responses for the question.
     * @return The possible responses for the question.
     */
    [[nodiscard]] virtual vector<string> getResponsePossible() const = 0;

    /**
     * @brief Gets the possible next questions based on the response.
     * @return The possible next questions based on the response.
     */
    [[nodiscard]] virtual vector<shared_ptr<Question>> getPossibleNextQuestions() const = 0;

    string id;                       ///< The unique identifier of the question.
    QuestionType::Type questionType; ///< The type of question.
    string questionText;             ///< The text of the question.
};

#endif // HATCHCARE_QUESTION_H
