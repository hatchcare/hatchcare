/**
 * @file QuestionDate.h
 * @brief The file contains the declaration of the QuestionDate class.
 *
 * The QuestionDate class inherits from the Question class and provides additional functionality
 * for handling date-based questions. It allows specifying the precision of the date (year, month, day,
 * hour, minute, or second) and provides methods for getting the response type and possible responses.
 *
 */

#ifndef HATCHCARE_QUESTIONHOUR_H
#define HATCHCARE_QUESTIONHOUR_H

#include <ctime>
#include <ostream>
#include <cassert>
#include "Question.h"
#include "../common.h"

/**
 * @brief Represents a date-based question.
 *
 * This class inherits from the base class Question and provides additional functionality
 * for handling date-based questions. It allows specifying the precision of the date
 * (year, month, day, hour, minute, or second) and provides methods for getting the
 * response type and possible responses.
 */
class QuestionHour : public Question {
public:
    /**
     * @brief Constructs a QuestionDate object with the specified parameters.
     *
     * @param id The ID of the question.
     * @param questionType The type of the question.
     * @param questionText The text of the question.
     * @param nextQuestionMap A map of possible next questions based on the response.
     * @param precision The precision of the date (0 for year, 1 for month, 2 for day,
     *                  3 for hour, 4 for minute, 5 for second).
     */
    QuestionHour(const string &id, QuestionType::Type questionType, const string &questionText,
                 const map<pair<int, int>, pair<shared_ptr<Question>, AnimationTriggers::Type>> &nextQuestionMap,
                 int precision);

    /**
     * @brief Default constructor.
     */
    QuestionHour();

    /**
     * @brief Destructor.
     */
    ~QuestionHour() override = default;

    /**
     * @brief Gets the response type of the question.
     *
     * @return The response type, which is "date".
     */
    [[nodiscard]] string getReponseType() const override {
        return "hour";
    }

    /**
     * @brief Save the reponse into the reponse object
     * @param reponse The response object associated with the question.
     * @param response The user's response to the question.
     * @return The next question to ask
     */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    saveReponse(Reponse *reponse, reponseTypes response) const override;

    /**
     * @brief Gets the possible responses for the question.
     *
     * @return A vector of strings representing the possible responses.
     */
    [[nodiscard]] vector<string> getResponsePossible() const override;

    /**
     * @brief Asks the question in the console and returns the next question based on the response.
     *
     * @param reponse A pointer to the response object.
     * @return An optional shared pointer to the next question, or an empty optional if there is no next question.
     */
    optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> askConsole(Reponse *reponse) const override;

    /**
     * @brief Overloaded stream insertion operator for outputting the question to an ostream.
     *
     * @param os The output stream.
     * @param questionDate The QuestionDate object to be outputted.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const QuestionHour &questionDate);

    /**
     * @brief Gets the possible next questions based on the response.
     *
     * @return A vector of shared pointers to the possible next questions.
     */
    [[nodiscard]] vector<shared_ptr<Question>> getPossibleNextQuestions() const override;

    /**
     * @brief Gets the precision of the date.
     *
     * @return The precision of the date (0 for year, 1 for month, 2 for day, 3 for hour, 4 for minute, 5 for second).
     */
    [[maybe_unused]] [[nodiscard]] int getPrecision() const;

private:
    /**
     * @brief A map that stores the next question and its associated animation trigger based on a time value.
     *
     * The keys of the map are time values represented as a `pair` of `int` representing the time limits in seconds, and the values are pairs consisting of a shared pointer to a `Question` object and an `AnimationTriggers::Type` value.
     */
    map<pair<int, int>, pair<shared_ptr<Question>, AnimationTriggers::Type>> nextQuestionMap;
    int precision; /// 0 for hour, 1 for minute, 2 for second
};

#endif // HATCHCARE_QUESTIONHOUR_H
