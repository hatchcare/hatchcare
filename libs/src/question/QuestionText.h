/**
 * @file QuestionText.h
 * @brief Header file for the QuestionText class.
 *
 * This file contains the declaration of the QuestionText class, which represents a text-based question.
 *
 * The QuestionText class inherits from the base class Question and provides functionality to handle text-based questions.
 *
 * The class provides the following functionality:
 * - Ask the question on the console and return the next question based on the user's response.
 * - Get the response type of the question.
 * - Overloaded stream insertion operator for QuestionText.
 * - Equality and inequality comparison operators for QuestionText.
 * - Get the possible response options for the question.
 * - Get the possible next questions based on the current question.
 */

#ifndef HATCHCARE_QUESTIONTEXT_H
#define HATCHCARE_QUESTIONTEXT_H

#include <ostream>
#include "Question.h"
#include <cassert>
#include "../common.h"

/**
 * @class QuestionText
 * @brief Represents a text-based question.
 *
 * This class inherits from the base class Question and provides functionality to handle text-based questions.
 */
class QuestionText : public Question {
public:
    /**
     * @brief Constructs a QuestionText object with the specified parameters.
     * @param id The ID of the question.
     * @param questionType The type of the question.
     * @param questionText The text of the question.
     * @param nextQuestion A map of possible next questions.
     */
    QuestionText(const string &id, QuestionType::Type questionType, const string &questionText,
                 const pair<shared_ptr<Question>, AnimationTriggers::Type> &nextQuestion);


    /**
     * @brief Asks the question on the console and returns the next question based on the user's response.
     * @param reponse A pointer to the response object.
     * @return An optional shared pointer to the next question.
     */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    askConsole(Reponse *reponse) const override;

    /**
     * @brief Gets the response type of the question.
     * @return The response type.
     */
    [[nodiscard]] string getReponseType() const override {
        return "text";
    }

    /**
 * @brief Save the reponse into the reponse object
 * @param reponse The response object associated with the question.
 * @param response The user's response to the question.
 * @return The next question to ask
 */
    [[nodiscard]] optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
    saveReponse(Reponse *reponse, reponseTypes response) const override;

    /**
     * @brief Overloaded stream insertion operator for QuestionText.
     * @param os The output stream.
     * @param text The QuestionText object to be inserted.
     * @return The output stream.
     */
    friend ostream &operator<<(ostream &os, const QuestionText &text);


    /**
     * @brief Gets the possible response options for the question.
     * @return A vector of response options.
     */
    [[nodiscard]] vector<string> getResponsePossible() const override;

    /**
     * @brief Gets the possible next questions based on the current question.
     * @return A vector of possible next questions.
     */
    [[nodiscard]] vector<shared_ptr<Question>> getPossibleNextQuestions() const override;

private:
    pair<shared_ptr<Question>, AnimationTriggers::Type> nextQuestion; /// The map of possible next questions.
};

#endif // HATCHCARE_QUESTIONTEXT_H
