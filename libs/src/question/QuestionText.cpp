/**
 * @file QuestionText.cpp
 * @brief Implementation file for the QuestionText class.
 */

#include "QuestionText.h"

QuestionText::QuestionText(const string &id, QuestionType::Type questionType, const string &questionText,
                           const pair<shared_ptr<Question>, AnimationTriggers::Type> &nextQuestion)
        : Question(id,
                   questionType,
                   questionText),
          nextQuestion(
                  nextQuestion) {

    assert(questionType == QuestionType::rant ||
           questionType == QuestionType::sleepPlus ||
           questionType == QuestionType::mealPlus);
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> QuestionText::askConsole(Reponse *reponse) const {
    displayText(questionText + "\n");
    string response;
    displayText(questionText);
    cout << ANSWER_ARROW;
    cin >> response;
    return saveReponse(reponse, response);
}

vector<string> QuestionText::getResponsePossible() const {
    return {"everything is fine don't worry be happy"};
}

vector<shared_ptr<Question>> QuestionText::getPossibleNextQuestions() const {
    if (nextQuestion.first != nullptr) {
        return {nextQuestion.first};
    }
    return {};
}

ostream &operator<<(ostream &os, const QuestionText &text) {
    os << static_cast<const Question &>(text) << " nextQuestionMap: ";
    os << "everything point to : " << text.nextQuestion.first << " ";
    return os;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
QuestionText::saveReponse(Reponse *reponse, reponseTypes response) const {
    cout << "on repond dans les texts coucouuuu" << endl;
    cout << "response: " << get<string>(response) << endl;
    switch (questionType) {
        case QuestionType::rant:
            reponse->rant = get<string>(response);
            break;
        case QuestionType::mealPlus:
            reponse->mealPlus = get<string>(response);
            break;
        case QuestionType::sleepPlus:
            reponse->sleepPlus = get<string>(response);
            break;
        default:
            throw invalid_argument("Invalid question type");
    }
    cout << nextQuestion.first << endl;
    if (nextQuestion.first == nullptr) {
        return nullopt;
    }
    return {nextQuestion};
}
