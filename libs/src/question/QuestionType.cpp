#include <stdexcept>
#include <ctime>
#include "QuestionType.h"
#include "QuestionAdjective.h"

string QuestionType::to_string(QuestionType::Type type) {
    switch (type) {
        case noteMood:
            return "noteMood";
        case adjective:
            return "adjective";
        case rant:
            return "rant";
        case sleepBegin:
            return "sleepBegin";
        case sleepEnd:
            return "sleepEnd";
        case sleepQuality:
            return "sleepQuality";
        case sleepPlus:
            return "sleepPlus";
        case mealHour:
            return "mealHour";
        case mealQuality:
            return "mealQuality";
        case mealPlus:
            return "mealPlus";
        default:
            return "unknown";
    }
}

ReponseType::Type QuestionType::toReponseType(QuestionType::Type type) {
    switch (type) {
        case noteMood:
        case sleepQuality:
        case mealQuality:
            return ReponseType::number;
        case adjective:
            return ReponseType::adjective;
        case rant:
        case sleepPlus:
        case mealPlus:
            return ReponseType::text;
        case sleepBegin:
        case sleepEnd:
        case mealHour:
            return ReponseType::date;
        default:
            throw invalid_argument("invalid question type encountered");
    }
}

bool QuestionType::isAnalysable(Type type) {
    switch (type) {
        case noteMood:
        case sleepBegin:
        case sleepEnd:
        case sleepQuality:
        case mealHour:
        case mealQuality:
        case adjective:
            return true;
        default:
            return false;
    }
}


int QuestionType::toInt(QuestionType::Type type, reponseTypes value) {
    switch (type) {
        case noteMood:
        case sleepQuality:
        case mealQuality:
        case sleepBegin:
        case sleepEnd:
        case mealHour:
            return get<int>(value);
        case adjective:
            //find the adjective category and then the index in this category
            for (const auto &pair: adjectivesSubList) {
                auto iterator = find(pair.second.begin(), pair.second.end(), get<string>(value));
                if (iterator != pair.second.end()) {
                    return (int) distance(pair.second.begin(), iterator);
                }
            }
            throw invalid_argument("invalid adjective encountered, not int eh list ");
        default:
            throw invalid_argument("type is not a valid int type");
    }
}
