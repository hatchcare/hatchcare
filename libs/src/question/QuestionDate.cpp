/**
 * @file QuestionDate.cpp
 * @brief Implementation file for the QuestionDate class.
 */

#include <sstream>
#include <iomanip>
#include <valarray>
#include "QuestionDate.h"

QuestionDate::QuestionDate(const string &id, QuestionType::Type questionType, const string &questionText,
                           const map<time_t, pair<shared_ptr<Question>, AnimationTriggers::Type>> &nextQuestionMap,
                           int precision)
        : Question(id,
                   questionType,
                   questionText),
          nextQuestionMap(
                  nextQuestionMap),
          precision(
                  precision) {
//    assert(questionType == QuestionType::date) there is still no question type with this type
    assert(precision >= 0 && precision <= 5);
}

QuestionDate::QuestionDate() {
    precision = 0;
}

vector<string> QuestionDate::getResponsePossible() const {
    switch (precision) {
        case 0:
            return {"YYYY"};
        case 1:
            return {"YYYY-MM"};
        case 2:
            return {"YYYY-MM-DD"};
        case 3:
            return {"YYYY-MM-DD HH"};
        case 4:
            return {"YYYY-MM-DD HH:MM"};
        case 5:
            return {"YYYY-MM-DD HH:MM:SS"};
        default:
            throw invalid_argument("Invalid precision");
    }
}

vector<shared_ptr<Question>> QuestionDate::getPossibleNextQuestions() const {
    vector<shared_ptr<Question>> result = {};
    for (auto &i: nextQuestionMap) {
        result.push_back(nextQuestionMap.at(i.first).first);
    }
    return result;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> QuestionDate::askConsole(Reponse *reponse) const {
    displayText(questionText + "\n");
    string rawReponse;
    string format;
    string formatDisplayed;
    switch (precision) {
        case 0:
            format = "%Y";
            formatDisplayed = "YYYY";
            break;
        case 1:
            format = "%Y-%m";
            formatDisplayed = "YYYY-MM";
            break;
        case 2:
            format = "%Y-%m-%d";
            formatDisplayed = "YYYY-MM-DD";
            break;
        case 3:
            format = "%Y-%m-%d %H";
            formatDisplayed = "YYYY-MM-DD HH";
            break;
        case 4:
            format = "%Y-%m-%d %H:%M";
            formatDisplayed = "YYYY-MM-DD HH:MM";
            break;
        case 5:
            format = "%Y-%m-%d %H:%M:%S";
            formatDisplayed = "YYYY-MM-DD HH:MM:SS";
            break;
        default:
            throw invalid_argument("Invalid precision");
    }

    tm tm = {};
    istringstream ss;
    do {
        displayText("Veuillez entrer une date au format " + formatDisplayed + "\n");
        cout << ANSWER_ARROW;
        cin >> rawReponse;
        ss = istringstream(rawReponse);
        ss >> get_time(&tm, format.c_str());
        if (ss.fail()) {
            displayText("Parse failed\n");
        }
    } while (ss.fail());
    time_t time = mktime(&tm);
    cout << time << endl;

    return saveReponse(reponse, time);
}

ostream &operator<<(ostream &os, const QuestionDate &questionDate) {
    os << static_cast<const Question &>(questionDate) << " nextQuestionMap: ";
    for (auto &i: questionDate.nextQuestionMap) {
        os << i.first << " : " << i.second.first << " ";
    }
    os << " precision: " << questionDate.precision;
    return os;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
QuestionDate::saveReponse(Reponse *reponse, reponseTypes response) const {
    switch (questionType) {
        default:
            throw invalid_argument("Invalid question type");
    }
    auto nextQuestion = nextQuestionMap.find(get<time_t>(response));
    if (nextQuestion == nextQuestionMap.end()) {
        return nullopt;
    }
    return nextQuestion->second;
}

int QuestionDate::getPrecision() const {
    return precision;
}
