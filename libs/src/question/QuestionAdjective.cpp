/**
 * @file QuestionAdjective.cpp
 * @brief Implementation file for the QuestionAdjective class.
 */

#include "QuestionAdjective.h"

QuestionAdjective::QuestionAdjective(const string &id, QuestionType::Type questionType, const string &questionText,
                                     const map<string, pair<shared_ptr<Question>, AnimationTriggers::Type>> &reponseMap)
        : Question(id, questionType, questionText), reponseMap(reponseMap) {
    assert(questionType == QuestionType::adjective);
}

QuestionAdjective::QuestionAdjective() {
    questionType = QuestionType::adjective;
    questionText = "Quel adjectif te correspond le plus ?";
}


optional<pair<shared_ptr<Question>, AnimationTriggers::Type>> QuestionAdjective::askConsole(Reponse *reponse) const {
    // loop until the user enters a valid adjective number
    displayText(questionText + "\n");
    int intReponse = -1;
    do {
        for (int i = 0; i < ADJECTIVES_SIZE; i++) {
            displayText(to_string(i) + " : " + adjectivesList[i] + "\n");
        }
        cout << ANSWER_ARROW;
        cin >> intReponse;
    } while (!(intReponse < ADJECTIVES_SIZE && intReponse >= 0));
    vector<string> subAdjectives = adjectivesSubList.at(adjectivesList[intReponse]);
    intReponse = 0;
    string findResult;
    do {
        for (int i = 0; i < subAdjectives.size(); i++) {
            displayText(to_string(i) + " : " + subAdjectives[i] + "\n");
        }
        cout << ANSWER_ARROW;
        cin >> intReponse;
        findResult = subAdjectives.at(intReponse - 1);
    } while (findResult == subAdjectives.at(subAdjectives.size() - 1));

    auto val = saveReponse(reponse, findResult);
    return val;
}

vector<string> QuestionAdjective::getResponsePossible() const {
    vector<string> result;
    for (auto &i: adjectivesList) {
        result.push_back(i);
    }
    return result;
}

vector<shared_ptr<Question>> QuestionAdjective::getPossibleNextQuestions() const {
    vector<shared_ptr<Question>> result;
    for (auto &i: reponseMap) {
        result.push_back(i.second.first);
    }
    return result;
}

ostream &operator<<(ostream &os, const QuestionAdjective &questionAdjective) {
    os << static_cast<const Question &>(questionAdjective) << " reponseMap: ";
    for (auto &i: questionAdjective.reponseMap) {
        os << i.first << " : " << i.second.first << " ";
    }
    return os;
}

optional<pair<shared_ptr<Question>, AnimationTriggers::Type>>
QuestionAdjective::saveReponse(Reponse *reponse, reponseTypes response) const {
    if (questionType == QuestionType::adjective) {
        reponse->adjective = get<string>(response);
    }
    auto nextQuestion = reponseMap.find(get<string>(response));
    if (nextQuestion == reponseMap.end()) {
        string mainCategory;
        for (auto i: adjectivesSubList) {
            if (find(i.second.begin(), i.second.end(), get<string>(response)) != i.second.end()) {
                mainCategory = i.first;
                break;
            }
        }
        nextQuestion = reponseMap.find(mainCategory);
        if (nextQuestion == reponseMap.end()) {
            return nullopt;
        }
    }
    return nextQuestion->second;
}

