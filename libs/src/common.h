/**
 * @file common.h
 * @brief Header file for common functions
 *
 * This file contains declarations for common functions used in the project.
 * It includes the necessary headers and defines the function prototypes.
 */

#ifndef HATCHCARE_COMMON_H
#define HATCHCARE_COMMON_H
#define ANSWER_ARROW "==> "

#include <iostream>
#include <thread>

using namespace std;
using namespace this_thread; // sleep_for, sleep_until
using namespace chrono;

class AnimationTriggers {
public:
    enum Type {
        to_neutral,
        to_thinking,
        to_excited,
        to_sad,
        to_oh,
        to_mad,
        to_happy,
    };
    // define value to be able to iterate over the enum
    static constexpr Type values[] = {to_neutral, to_thinking, to_excited, to_sad, to_oh, to_mad, to_happy};

    /**
     * @brief Converts the given AnimationTriggers::Type to a string representation.
     *
     * This function takes an AnimationTriggers::Type enum value and converts it to a string representation.
     *
     * @param type The AnimationTriggers::Type value to be converted.
     * @return The string representation of the AnimationTriggers::Type value.
     */
    static string to_string(Type type) {
        switch (type) {
            case to_neutral:
                return "to_neutral";
            case to_thinking:
                return "to_thinking";
            case to_excited:
                return "to_excited";
            case to_sad:
                return "to_sad";
            case to_oh:
                return "to_oh";
            case to_mad:
                return "to_mad";
            case to_happy:
                return "to_happy";
            default:
                return "Unknown";
        }
    }
};

/**
 * @brief Displays the given text.
 *
 * This function takes a string as input and displays it on the console.
 *
 * @param txt The text to be displayed.
 */
void displayText(string txt);

/**
 * @brief Converts the given date to a string representation.
 *
 * This function takes a time_t object representing a date and converts it to a string.
 *
 * @param date The date to be converted.
 * @return The string representation of the date.
 */
string dateToString(const time_t &date);

/*
 * @brief Converts the given string to a time_t object.
 * @param The string to be converted, the things to replace and the values to replace them with
 * @return the string with the replaced values
 */
string reblaceAll(string str, const string &from, const string &to);

#endif // HATCHCARE_COMMON_H
