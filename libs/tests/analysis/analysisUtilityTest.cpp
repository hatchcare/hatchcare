#include "gtest/gtest.h"
#include "../../src/reponse/AnalysisUtility.hpp"

TEST(TestRanking, simpleRank) {
    std::vector<int> data = {2, 5, 7, 12, 2};
    auto result = AnalysisUtility::rankData(data);
    std::vector<std::pair<int, double>> attendedResult = {
            std::make_pair(2, 4.5),
            std::make_pair(5, 3),
            std::make_pair(7, 2),
            std::make_pair(12, 1),
    };

}


TEST(TestRanking, complexeRank) {
    std::vector<int> data = {2, 5, 7, 12, 2, 5, 6, 8, 9, 7, 2, 3, 6, 4, 8};
    auto result = AnalysisUtility::rankData(data);
    std::vector<std::pair<int, double>> attendedResult = {
            std::make_pair(2, 14),
            std::make_pair(5, 9.5),
            std::make_pair(7, 5.5),
            std::make_pair(12, 1),
            std::make_pair(2, 14),
            std::make_pair(5, 9.5),
            std::make_pair(6, 7.5),
            std::make_pair(8, 3.5),
            std::make_pair(9, 2),
            std::make_pair(7, 5.5),
            std::make_pair(2, 14),
            std::make_pair(3, 12),
            std::make_pair(6, 7.5),
            std::make_pair(4, 11),
            std::make_pair(8, 3.5)
    };
}


TEST(TestCorrelation, simpleSpearman) {
    std::vector<int> data1 = {35, 23, 47, 17, 10, 43, 9, 6, 28};
    std::vector<int> data2 = {30, 33, 45, 23, 8, 49, 12, 4, 31};
    auto result = AnalysisUtility::spearmanCorrelation(data1, data2);
    ASSERT_EQ(result, 0.9);
}