#include <gtest/gtest.h>
#include "../src/question/Question.h"
#include "../src/question/QuestionNumber.h"
#include "../src/question/QuestionText.h"
#include "../src/question/QuestionAdjective.h"
#include "../src/question/QuestionDate.h"

TEST(questionTest, mermaid) {
    shared_ptr<QuestionDate> q1 = make_shared<QuestionDate>();

    auto q4 = QuestionText("q4",
                           QuestionType::rant,
                           "questionText4",
                           {});

    auto q3 = QuestionAdjective("q3",
                                QuestionType::adjective,
                                "questionText3",
                                {
                                        {"haha", pair(q1, AnimationTriggers::to_happy)}
                                });

    auto q2 = QuestionNumber("q2",
                             QuestionType::noteMood,
                             "questionText2",
                             {
                                     {1, pair(make_shared<QuestionAdjective>(q3), AnimationTriggers::to_happy)}
                             },
                             0,
                             5,
                             1);

    *q1 = QuestionDate("q1",
                       QuestionType::sleepBegin,
                       "questionText",
                       {
                               {737761, pair(std::make_shared<QuestionNumber>(q2), AnimationTriggers::to_happy)},
                               {737762, pair(std::make_shared<QuestionAdjective>(q3), AnimationTriggers::to_happy)},
                               {737763, pair(std::make_shared<QuestionText>(q4), AnimationTriggers::to_happy)}
                       },
                       0);


    std::string result = q1->toMermaid();
    ASSERT_EQ(result, "classDiagram\n"
                      "class q1 {\n"
                      "questionType : sleepBegin\n"
                      "question : questionText\n"
                      "responsePossible : [YYYY]\n"
                      "ReponseType:Type : date\n"
                      "questionType : sleepBegin\n"
                      "}\n"
                      "q2<|--q1\n"
                      "class q2 {\n"
                      "questionType : noteMood\n"
                      "question : questionText2\n"
                      "responsePossible : [0, 1, 2, 3, 4]\n"
                      "ReponseType:Type : number\n"
                      "questionType : noteMood\n"
                      "}\n"
                      "q3<|--q2\n"
                      "class q3 {\n"
                      "questionType : adjective\n"
                      "question : questionText3\n"
                      "responsePossible : [content, confiant, triste, en colère, paniqué, fatigué]\n"
                      "ReponseType:Type : adjective\n"
                      "questionType : adjective\n"
                      "}\n"
                      "q1<|--q3\n"
                      "q3<|--q1\n"
                      "q4<|--q1\n"
                      "class q4 {\n"
                      "questionType : rant\n"
                      "question : questionText4\n"
                      "responsePossible : [everything is fine don't worry be happy]\n"
                      "ReponseType:Type : text\n"
                      "questionType : rant\n"
                      "}\n"
    );
}