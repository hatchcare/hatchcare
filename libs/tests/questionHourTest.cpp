#include "gtest/gtest.h"
#include "../src/question/QuestionHour.h"
#include "../src/question/QuestionNumber.h"

TEST(QuestionHourTest, askConsole) {
    // Mock cin to input 2020-12-12
    std::istringstream mockInput("09:00");
    std::streambuf *originalCin = std::cin.rdbuf();
    std::cin.rdbuf(mockInput.rdbuf());

    std::map<pair<int, int>, std::pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
            {pair(28800, 32400), pair(make_shared<QuestionNumber>("q2", QuestionType::noteMood, "question 2",
                                                                  std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                                  0, 5, 1), AnimationTriggers::to_happy)},
    };
    std::shared_ptr<Question> q1 = make_shared<QuestionHour>("q1", QuestionType::sleepBegin, "question 1",
                                                             reponseMap, 1);
    auto *reponse = new Reponse();
    auto nextQuestion = q1->askConsole(reponse);
    ASSERT_TRUE(nextQuestion.has_value());
    ASSERT_EQ(nextQuestion.value().first->id, "q2");
    ASSERT_TRUE(reponse->sleepBegin.value() == 32400);

}
