#include "gtest/gtest.h"
#include "../src/question/QuestionAdjective.h"

TEST(QuestionAdjectifTest, askConsole) {
    // Mock cin to input 0 and then 1
    std::istringstream mockInput("0\n1");
    std::streambuf *originalCin = std::cin.rdbuf();
    std::cin.rdbuf(mockInput.rdbuf());

    std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
            {"détendu", pair(make_shared<QuestionAdjective>("q2", QuestionType::adjective, "question 2",
                                                            std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                             AnimationTriggers::to_happy)},
            {"triste",  pair(make_shared<QuestionAdjective>("q3", QuestionType::adjective, "question 3",
                                                            std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                             AnimationTriggers::to_mad)}

    };
    std::shared_ptr<Question> q1 = make_shared<QuestionAdjective>("q1", QuestionType::adjective, "question 1",
                                                                  reponseMap);
    auto *reponse = new Reponse();
    auto nextQuestion = q1->askConsole(reponse);

    ASSERT_TRUE(nextQuestion.has_value());
    ASSERT_EQ(nextQuestion.value().first->id, "q2");
    ASSERT_EQ(reponse->adjective, "détendu");

    //Test that main categorie work
    //mock cin to 3 and then 1
    std::cin.rdbuf(originalCin);
    std::istringstream mockInput1("2\n1");
    std::cin.rdbuf(mockInput1.rdbuf());

    auto tmp = q1->askConsole(reponse);
    ASSERT_TRUE(tmp.has_value());
    ASSERT_EQ(tmp.value().first->id, "q3");
    ASSERT_EQ(reponse->adjective, "déprimé");

    //mock cin to 3 and then 4
    std::cin.rdbuf(originalCin);
    std::istringstream mockInput2("3\n4");
    std::cin.rdbuf(mockInput2.rdbuf());

    nextQuestion = q1->askConsole(reponse);
    ASSERT_FALSE(nextQuestion.has_value());
    ASSERT_EQ(reponse->adjective, "exaspéré");

    // Restore cin
    std::cin.rdbuf(originalCin);
}

TEST(QuestionAdjectifTest, getResponsePossible) {
    std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {};
    std::shared_ptr<Question> q1 = make_shared<QuestionAdjective>("q1", QuestionType::adjective, "question 1",
                                                                  reponseMap);
    std::vector<std::string> result = q1->getResponsePossible();
    ASSERT_EQ(result.size(), 6);
    ASSERT_EQ(result[0], "content");
    ASSERT_EQ(result[1], "confiant");
    ASSERT_EQ(result[2], "triste");
    ASSERT_EQ(result[3], "en colère");
    ASSERT_EQ(result[4], "paniqué");
    ASSERT_EQ(result[5], "fatigué");
}

TEST(QuestionAdjectifTest, getPossibleNextQuestions) {
    std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
            {"détendu",   pair(make_shared<QuestionAdjective>("q2", QuestionType::adjective, "question 2",
                                                              std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                               AnimationTriggers::to_happy)},
            {"confiant",  pair(make_shared<QuestionAdjective>("q3", QuestionType::adjective, "question 3",
                                                              std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                               AnimationTriggers::to_happy)},
            {"triste",    pair(make_shared<QuestionAdjective>("q4", QuestionType::adjective, "question 4",
                                                              std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                               AnimationTriggers::to_happy)},
            {"en colère", pair(make_shared<QuestionAdjective>("q5", QuestionType::adjective, "question 5",
                                                              std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                               AnimationTriggers::to_happy)},
            {"paniqué",   pair(make_shared<QuestionAdjective>("q6", QuestionType::adjective, "question 6",
                                                              std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                               AnimationTriggers::to_happy)},
            {"fatigué",   pair(make_shared<QuestionAdjective>("q7", QuestionType::adjective, "question 7",
                                                              std::map<string, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>()),
                               AnimationTriggers::to_happy)},
    };
    std::shared_ptr<Question> q1 = make_shared<QuestionAdjective>("q1", QuestionType::adjective, "question 1",
                                                                  reponseMap);
    std::vector<std::shared_ptr<Question>> result = q1->getPossibleNextQuestions();
    ASSERT_EQ(result.size(), 6);
    for (int i = 0; i < 6; i++) {
        ASSERT_TRUE((result[i]->id == "q2") || (result[i]->id == "q3") || (result[i]->id == "q4") ||
                    (result[i]->id == "q5") || (result[i]->id == "q6") || (result[i]->id == "q7"));
    }
}