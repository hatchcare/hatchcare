#include <gtest/gtest.h>
#include "../src/reponse/Reponse.h"
#include "../src/reponse/ReponseList.h"

/**
* @class reponseTest
*/

TEST(ReponseTest, testReponse) {
    auto date = time(nullptr);
    auto noteMood = nullopt;
    auto adjectif = "adjectif";
    auto rant = ",ra,nt,";
    auto sommeilDebut = time(nullptr);
    auto sommeilFin = time(nullptr);
    auto sommeilQualite = 5;
    auto sommeilPlus = "sommeilPlus";
    auto repasHoraire = time(nullptr);
    auto repasQualite = 5;
    auto repasPlus = "repasPlus";

    auto reponse = Reponse(date, noteMood, adjectif, rant, sommeilDebut, sommeilFin, sommeilQualite, sommeilPlus,
                           repasHoraire, repasQualite, repasPlus);

    string csv = reponse.toCSV();

    std::cout << csv << std::endl;

    auto reponse2 = Reponse::fromCSV(csv);

    ASSERT_EQ(reponse, reponse2);
}

/**
* @brief Gestion de l'automatisation des types de question
*/
TEST(ReponseTest, testReponseList) {
    auto date = time(nullptr);
    auto noteMood = 5;
    auto adjectif = "adjectif";
    auto rant = "rant,";
    auto sommeilDebut = time(nullptr);
    auto sommeilFin = time(nullptr);
    auto sommeilQualite = 5;
    auto sommeilPlus = "sommeilPlus";
    auto repasHoraire = time(nullptr);
    auto repasQualite = 5;
    auto repasPlus = "repasPlus";

    auto reponse = Reponse(date, noteMood, adjectif, rant, sommeilDebut, sommeilFin, sommeilQualite, sommeilPlus,
                           repasHoraire, repasQualite, repasPlus);

    ReponseList reponseList({reponse});

    for (int i = 0; i < 10; ++i) {
        reponseList.addReponse(reponse);
    }

    ASSERT_EQ(reponseList.getReponse(0), reponse);
    ASSERT_EQ(reponseList.size(), 11);

    string csv = reponseList.toCSV();

    auto reponseList2 = ReponseList::fromCSV(csv);

    ASSERT_EQ(reponseList, reponseList2);
}