#include "gtest/gtest.h"
#include "../src/question/QuestionDate.h"
#include "../src/question/QuestionNumber.h"

// this test now does not make sense because we don't have any questionDate now
//TEST(QuestionDateTest, askConsole) {
//    // Mock cin to input 2020-12-12
//    std::istringstream mockInput("2020-12-12");
//    std::streambuf *originalCin = std::cin.rdbuf();
//    std::cin.rdbuf(mockInput.rdbuf());
//
//    std::map<time_t, std::pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
//            {1,          pair(make_shared<QuestionNumber>("q2", QuestionType::noteMood, "question 2",
//                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
//                                                          0, 5, 1), AnimationTriggers::to_happy)},
//            {2,          pair(make_shared<QuestionNumber>("q3", QuestionType::noteMood, "question 3",
//                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
//                                                          0, 5, 1), AnimationTriggers::to_happy)},
//            {4,          pair(make_shared<QuestionNumber>("q5", QuestionType::noteMood, "question 5",
//                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
//                                                          0, 5, 1), AnimationTriggers::to_happy)},
//            {5,          pair(make_shared<QuestionNumber>("q6", QuestionType::noteMood, "question 6",
//                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
//                                                          0, 5, 1), AnimationTriggers::to_happy)},
//            {1607727600, pair(make_shared<QuestionNumber>("q7", QuestionType::noteMood, "question 7",
//                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
//                                                          0, 5, 1), AnimationTriggers::to_happy)},
//            {1607731200, pair(make_shared<QuestionNumber>("q7", QuestionType::noteMood, "question 7",
//                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
//                                                          0, 5, 1), AnimationTriggers::to_happy)}
//
//    };
//    std::shared_ptr<Question> q1 = make_shared<QuestionDate>("q1", QuestionType::sleepBegin, "question 1",
//                                                             reponseMap, 2);
//    auto *reponse = new Reponse();
//    auto nextQuestion = q1->askConsole(reponse);
//    ASSERT_TRUE(nextQuestion.has_value());
//    ASSERT_EQ(nextQuestion.value().first->id, "q7");
////    ASSERT_TRUE(reponse->sleepBegin.value() == 1607727600 || reponse->sleepBegin.value() == 1607731200);
//
//
//    std::istringstream mockInput2("2020-12-13");
//    std::cin.rdbuf(originalCin);
//    std::cin.rdbuf(mockInput2.rdbuf());
//    nextQuestion = q1->askConsole(reponse);
//    ASSERT_FALSE(nextQuestion.has_value());
//    ASSERT_TRUE(reponse->sleepBegin.value() == 1607814000 || reponse->sleepBegin.value() == 1607817600);
//
//    q1 = make_shared<QuestionDate>("q1", QuestionType::sleepBegin, "question 1",
//                                   reponseMap, 5);
//
//    std::istringstream mockInput3("2020-12-13 12:00:00");
//    std::cin.rdbuf(originalCin);
//    std::cin.rdbuf(mockInput3.rdbuf());
//    nextQuestion = q1->askConsole(reponse);
//    ASSERT_FALSE(nextQuestion.has_value());
//    ASSERT_TRUE(reponse->sleepBegin.value() == 1607814000 || reponse->sleepBegin.value() == 1607817600);
//
//    // Restore cin
//    std::cin.rdbuf(originalCin);
//}

TEST(QuestionDateTest, getResponsePossible) {
    std::map<time_t, std::pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {};
    auto q1 = make_shared<QuestionDate>("q1", QuestionType::sleepBegin, "question 1",
                                        reponseMap, 5);
    std::vector<std::string> result = q1->getResponsePossible();
    ASSERT_EQ(result.size(), 1);
    ASSERT_EQ(result[0], "YYYY-MM-DD HH:MM:SS");
}

TEST(QuestionDateTest, getPossibleNextQuestions) {
    std::map<time_t, std::pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
            {1,          pair(make_shared<QuestionNumber>("q2", QuestionType::noteMood, "question 2",
                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                          0, 5, 1), AnimationTriggers::to_happy)},
            {2,          pair(make_shared<QuestionNumber>("q3", QuestionType::noteMood, "question 3",
                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                          0, 5, 1), AnimationTriggers::to_happy)},
            {4,          pair(make_shared<QuestionNumber>("q5", QuestionType::noteMood, "question 5",
                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                          0, 5, 1), AnimationTriggers::to_happy)},
            {5,          pair(make_shared<QuestionNumber>("q6", QuestionType::noteMood, "question 6",
                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                          0, 5, 1), AnimationTriggers::to_happy)},
            {1607727600, pair(make_shared<QuestionNumber>("q7", QuestionType::noteMood, "question 7",
                                                          std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                          0, 5, 1), AnimationTriggers::to_happy)}

    };
    std::shared_ptr<Question> q1 = make_shared<QuestionDate>("q1", QuestionType::sleepBegin, "question 1",
                                                             reponseMap, 2);
    std::vector<std::shared_ptr<Question>> result = q1->getPossibleNextQuestions();
    ASSERT_EQ(result.size(), 5);
    for (int i = 0; i < 5; i++) {
        ASSERT_TRUE((result[i]->id == "q2") || (result[i]->id == "q3") || (result[i]->id == "q4") ||
                    (result[i]->id == "q5") || (result[i]->id == "q6") || (result[i]->id == "q7"));
    }
}




/*
TEST(QuestionAdjectifTest, askConsole) {
// Mock cin to input 1
std::istringstream mockInput("1");
std::streambuf *originalCin = std::cin.rdbuf();
std::cin.rdbuf(mockInput.rdbuf());

std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
        {1, pair(make_shared<QuestionNumber>("q2", QuestionType::adjectif, "question 2",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1),AnimationTriggers::to_happy)},
        {2, pair(make_shared<QuestionNumber>("q3", QuestionType::adjectif, "question 3",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1),AnimationTriggers::to_happy)},
        {4, pair(make_shared<QuestionNumber>("q5", QuestionType::adjectif, "question 5",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1),AnimationTriggers::to_happy)},
        {5, pair(make_shared<QuestionNumber>("q6", QuestionType::adjectif, "question 6",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1)}

};
std::shared_ptr<Question> q1 = pair(make_shared<QuestionNumber>("q1", QuestionType::repasQualite, "question 1",
                                                           reponseMap, 0, 5, 1);
Reponse reponse;
std::optional<std::shared_ptr<Question>> nextQuestion = q1->askConsole(&reponse);
ASSERT_TRUE(nextQuestion.has_value());
ASSERT_EQ(nextQuestion.value()->id, "q2");
ASSERT_EQ(reponse.repasQualite, 1);

//mock cin to 3
std::cin.rdbuf(originalCin);
std::istringstream mockInput2("3");
std::cin.rdbuf(mockInput2.rdbuf());

nextQuestion = q1->askConsole(&reponse);
ASSERT_FALSE(nextQuestion.has_value());
ASSERT_EQ(reponse.repasQualite, 3);

// Restore cin
std::cin.rdbuf(originalCin);
}

TEST(QuestionAdjectifTest, getResponsePossible) {
std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {};
std::shared_ptr<Question> q1 = pair(make_shared<QuestionNumber>("q1", QuestionType::repasQualite, "question 1",
                                                           reponseMap, 0, 5, 1);
std::vector<std::string> result = q1->getResponsePossible();
ASSERT_EQ(result.size(), 5);
ASSERT_EQ(result[0], "0");
ASSERT_EQ(result[1], "1");
ASSERT_EQ(result[2], "2");
ASSERT_EQ(result[3], "3");
ASSERT_EQ(result[4], "4");
}

TEST(QuestionAdjectifTest, getPossibleNextQuestions) {
std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
        {1, pair(make_shared<QuestionNumber>("q2", QuestionType::adjectif, "question 2",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1),AnimationTriggers::to_happy)},
        {2, pair(make_shared<QuestionNumber>("q3", QuestionType::adjectif, "question 3",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1),AnimationTriggers::to_happy)},
        {4, pair(make_shared<QuestionNumber>("q5", QuestionType::adjectif, "question 5",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1),AnimationTriggers::to_happy)},
        {5, pair(make_shared<QuestionNumber>("q6", QuestionType::adjectif, "question 6",
                                        std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(), 0, 5, 1)}

};
std::shared_ptr<Question> q1 = pair(make_shared<QuestionNumber>("q1", QuestionType::repasQualite, "question 1",
                                                           reponseMap, 0, 5, 1);
std::vector<std::shared_ptr<Question>> result = q1->getPossibleNextQuestions();
ASSERT_EQ(result.size(), 4);
ASSERT_EQ(result[0]->id, "q2");
ASSERT_EQ(result[1]->id, "q3");
ASSERT_EQ(result[2]->id, "q5");
ASSERT_EQ(result[3]->id, "q6");
}
 */