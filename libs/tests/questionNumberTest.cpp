#include <gtest/gtest.h>

#include "../src/question/QuestionNumber.h"

TEST(QuestionNumberTest, askConsole) {
    // Mock cin to input 1
    std::istringstream mockInput("1");
    std::streambuf *originalCin = std::cin.rdbuf();
    std::cin.rdbuf(mockInput.rdbuf());

    std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
            {1, pair(make_shared<QuestionNumber>("q2", QuestionType::noteMood, "question 2",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)},
            {2, pair(make_shared<QuestionNumber>("q3", QuestionType::noteMood, "question 3",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)},
            {4, pair(make_shared<QuestionNumber>("q5", QuestionType::noteMood, "question 5",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)},
            {5, pair(make_shared<QuestionNumber>("q6", QuestionType::noteMood, "question 6",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)}

    };
    std::shared_ptr<Question> q1 = make_shared<QuestionNumber>("q1", QuestionType::mealQuality, "question 1",
                                                               reponseMap, 0, 5, 1);
    auto *reponse = new Reponse();
    auto nextQuestion = q1->askConsole(reponse);
    ASSERT_TRUE(nextQuestion.has_value());
    ASSERT_EQ(nextQuestion.value().first->id, "q2");
    ASSERT_EQ(reponse->mealQuality, 1);

    // mock cin to 3
    std::cin.rdbuf(originalCin);
    std::istringstream mockInput2("3");
    std::cin.rdbuf(mockInput2.rdbuf());

    nextQuestion = q1->askConsole(reponse);
    ASSERT_FALSE(nextQuestion.has_value());
    ASSERT_EQ(reponse->mealQuality, 3);

    // Restore cin
    std::cin.rdbuf(originalCin);
}

TEST(QuestionNumberTest, getResponsePossible) {
    std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {};
    std::shared_ptr<Question> q1 = make_shared<QuestionNumber>("q1", QuestionType::mealQuality, "question 1",
                                                               reponseMap, 0, 5, 1);
    std::vector<std::string> result = q1->getResponsePossible();
    ASSERT_EQ(result.size(), 5);
    ASSERT_EQ(result[0], "0");
    ASSERT_EQ(result[1], "1");
    ASSERT_EQ(result[2], "2");
    ASSERT_EQ(result[3], "3");
    ASSERT_EQ(result[4], "4");
}

TEST(QuestionNumberTest, getPossibleNextQuestions) {
    std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>> reponseMap = {
            {1, pair(make_shared<QuestionNumber>("q2", QuestionType::noteMood, "question 2",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)},
            {2, pair(make_shared<QuestionNumber>("q3", QuestionType::noteMood, "question 3",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)},
            {4, pair(make_shared<QuestionNumber>("q5", QuestionType::noteMood, "question 5",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)},
            {5, pair(make_shared<QuestionNumber>("q6", QuestionType::noteMood, "question 6",
                                                 std::map<int, pair<std::shared_ptr<Question>, AnimationTriggers::Type>>(),
                                                 0, 5, 1), AnimationTriggers::to_happy)}

    };
    std::shared_ptr<Question> q1 = make_shared<QuestionNumber>("q1", QuestionType::mealQuality, "question 1",
                                                               reponseMap, 0, 5, 1);
    std::vector<std::shared_ptr<Question>> result = q1->getPossibleNextQuestions();
    ASSERT_EQ(result.size(), 4);
    ASSERT_EQ(result[0]->id, "q2");
    ASSERT_EQ(result[1]->id, "q3");
    ASSERT_EQ(result[2]->id, "q5");
    ASSERT_EQ(result[3]->id, "q6");
}