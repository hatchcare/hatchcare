
#include "../../src/question/Question.h"
#include "../../src/question/QuestionNumber.h"
#include "../../src/question/QuestionAdjective.h"
#include "../../src/question/QuestionText.h"
#include "../../src/question/QuestionDate.h"

using namespace std;

const auto q10 = QuestionText("q10",
                              QuestionType::rant,
                              "Tu peux te rendre dans l'onglet Ressources pour trouver des pistes d'aide autour de toi. J'espère que tu iras vite mieux, je t'attends ici.",
                              {});

const auto q9 = QuestionText("q9",
                             QuestionType::rant,
                             "Prends ton temps pour aller mieux, je t'attends ici.",
                             {});

const auto q8 = QuestionText("q8",
                             QuestionType::rant,
                             "Génial !",
                             {});

const auto q7 = QuestionText("q7",
                             QuestionType::rant,
                             "Hey hey ! Plutôt pas mal !",
                             {});

const auto q6 = QuestionText("q6",
                             QuestionType::rant,
                             "Oh non... Est-ce que tu as des idées pour améliorer ça ?",
                             pair(make_shared<QuestionText>(q9), AnimationTriggers::to_excited));

const auto q5 = QuestionNumber("q5",
                               QuestionType::noteMood,
                               "Je vois... Si tu devais noter ton humeur sur une échelle de 0 à 10, tu donnerais...",
                               {
                                       {0,  pair(make_shared<QuestionText>(q6), AnimationTriggers::to_thinking)},
                                       {1,  pair(make_shared<QuestionText>(q6), AnimationTriggers::to_thinking)},
                                       {2,  pair(make_shared<QuestionText>(q6), AnimationTriggers::to_thinking)},
                                       {3,  pair(make_shared<QuestionText>(q6), AnimationTriggers::to_thinking)},
                                       {4,  pair(make_shared<QuestionText>(q7), AnimationTriggers::to_thinking)},
                                       {5,  pair(make_shared<QuestionText>(q7), AnimationTriggers::to_thinking)},
                                       {6,  pair(make_shared<QuestionText>(q7), AnimationTriggers::to_thinking)},
                                       {7,  pair(make_shared<QuestionText>(q8), AnimationTriggers::to_thinking)},
                                       {8,  pair(make_shared<QuestionText>(q8), AnimationTriggers::to_thinking)},
                                       {9,  pair(make_shared<QuestionText>(q8), AnimationTriggers::to_thinking)},
                                       {10, pair(make_shared<QuestionText>(q8), AnimationTriggers::to_thinking)},
                               },
                               0,
                               10,
                               1);

const auto q4 = QuestionText("q4",
                             QuestionType::rant,
                             "Est-ce que tu as pris le temps de te détendre avant de te coucher ?",
                             pair(make_shared<QuestionNumber>(q5), AnimationTriggers::to_excited));

const auto q3 = QuestionText("q3",
                             QuestionType::rant,
                             "Comment est-ce que tu as dormi ?",
                             pair(make_shared<QuestionText>(q4),
                                  AnimationTriggers::to_excited));

const auto q2 = QuestionDate("q2",
                             QuestionType::sleepBegin,
                             "Quand t'es-tu couché ?",
                             {{737762, pair(std::make_shared<QuestionText>(q3), AnimationTriggers::to_happy)},
                              {737763, pair(std::make_shared<QuestionText>(q3), AnimationTriggers::to_happy)}},
                             4);

const auto q1 = QuestionAdjective("q1",
                                  QuestionType::adjective,
                                  "Hey ! Comment est-ce que tu te sens en ce moment ?",
                                  {
                                          {"content",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"détendu",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"motivé",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"heureux",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"joyeux",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"enthousiaste", pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"énergique",    pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},

                                          {"confiant",     pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"performant",   pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"beau",         pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"concentré",    pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"épanoui",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"fier",         pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},

                                          {"triste",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"déprimé",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"dépressif",    std::pair(make_shared<QuestionDate>(q2),
                                                                     AnimationTriggers::to_happy)},
                                          {"abattu",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"mélancolique", pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"coupable",     pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"seul",         pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},

                                          {"en colère",    pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"furieux",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"tendu",        pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"agité",        pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"exaspéré",     pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"enragé",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},

                                          {"paniqué",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"peureux",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"nerveux",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"phobique",     pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"inquiet",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"angoissé",     pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},

                                          {"fatigué",      pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"épuisé",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"somnolant",    pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"engourdi",     pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"usé",          pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                          {"ennuyé",       pair(make_shared<QuestionDate>(q2),
                                                                AnimationTriggers::to_happy)},
                                  });
