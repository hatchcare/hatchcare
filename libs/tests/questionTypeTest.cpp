#include <gtest/gtest.h>
#include "../src/question/QuestionType.h"

TEST(QuestionTest, testToInt) {
    // Test case 1: Testing sommeilDebut type
    QuestionType::Type type1 = QuestionType::sleepBegin;
    reponseTypes value1;
    std::istringstream ss1("23:12:34");
    std::tm t = {};
    ss1 >> std::get_time(&t, "%H:%M:%S");
    value1 = t.tm_hour * 60 + t.tm_min + t.tm_sec / 60;
    int expected1 = 1392;
    int result1 = QuestionType::toInt(type1, value1);
    ASSERT_EQ(result1, expected1);

    // Test case 6: Testing adjective
    QuestionType::Type type6 = QuestionType::adjective;
    reponseTypes value6 = "enthousiaste";
    int expected6 = 4;
    int result6 = QuestionType::toInt(type6, value6);
    ASSERT_EQ(result6, expected6);
}