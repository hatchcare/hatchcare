#include <gtest/gtest.h>
#include "../../src/c_wrapper/hatchcare_lib.h"


TEST(WrapperTestCorrelation, testCorrelationGetters) {
    auto correlationObject = Correlation(QuestionType::noteMood, QuestionType::sleepQuality, "adjective", 0.5, 1);
    auto correlation = correlation_t{&correlationObject};
    ASSERT_EQ(correlationGetQuestionTypeA(correlation), QuestionType::to_string(QuestionType::noteMood));
    ASSERT_EQ(correlationGetQuestionTypeB(correlation), QuestionType::to_string(QuestionType::sleepQuality));
    ASSERT_EQ(correlationGetIsAdjective(correlation), true);
    ASSERT_EQ(string(correlationGetAdjectiveCategory(correlation)), "adjective");
    ASSERT_EQ(correlationGetCoefficient(correlation), 0.5);
    ASSERT_EQ(correlationGetLag(correlation), 1);
}