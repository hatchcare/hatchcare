/**
 * @file questionDatas.h
 * @brief Fichier de données pour les questions
 */

#include "../src/question/Question.h"
#include "../src/question/QuestionNumber.h"
#include "../src/question/QuestionAdjective.h"
#include "../src/question/QuestionText.h"
#include "../src/question/QuestionDate.h"
#include "../src/question/QuestionHour.h"

#ifndef HATCHCARE_QUESTIONDATAS_H
#define HATCHCARE_QUESTIONDATAS_H

using namespace std;

/*const auto q11 = QuestionText("q11",
                              QuestionType::rant,
                              "Tu peux te rendre dans l'onglet Ressources pour trouver des pistes d'aide autour de toi. J'espère que tu iras vite mieux, je t'attends ici.",
                              {nullptr, AnimationTriggers::to_neutral});

const auto q10 = QuestionText("q10",
                              QuestionType::rant,
                              "Prends ton temps pour aller mieux, je t'attends ici.",
                              {nullptr, AnimationTriggers::to_neutral});
*/

const auto q13 = QuestionText("q13",
                              QuestionType::rant,
                              "Génial !",
                              {nullptr, AnimationTriggers::to_neutral});

const auto q12 = QuestionText("q12",
                              QuestionType::rant,
                              "Hey hey ! Plutôt pas mal !",
                              {nullptr, AnimationTriggers::to_neutral});

const auto q11 = QuestionText("q11",
                              QuestionType::rant,
                              "Prends ton temps pour aller mieux, je t'attends ici.",
                              {nullptr, AnimationTriggers::to_neutral});

const auto q10 = QuestionNumber("q10",
                                QuestionType::noteMood,
                                "Je vois... Si tu devais noter ton humeur sur une échelle croissante de 0 à 10, tu donnerais...",
                                {
                                    {0, pair(make_shared<QuestionText>(q11), AnimationTriggers::to_oh)},
                                    {1, pair(make_shared<QuestionText>(q11), AnimationTriggers::to_sad)},
                                    {2, pair(make_shared<QuestionText>(q11), AnimationTriggers::to_sad)},
                                    {3, pair(make_shared<QuestionText>(q11), AnimationTriggers::to_sad)},
                                    {4, pair(make_shared<QuestionText>(q12), AnimationTriggers::to_thinking)},
                                    {5, pair(make_shared<QuestionText>(q12), AnimationTriggers::to_thinking)},
                                    {6, pair(make_shared<QuestionText>(q12), AnimationTriggers::to_thinking)},
                                    {7, pair(make_shared<QuestionText>(q13), AnimationTriggers::to_thinking)},
                                    {8, pair(make_shared<QuestionText>(q13), AnimationTriggers::to_happy)},
                                    {9, pair(make_shared<QuestionText>(q13), AnimationTriggers::to_happy)},
                                    {10, pair(make_shared<QuestionText>(q13), AnimationTriggers::to_excited)},
                                },
                                0,
                                10,
                                1);

const auto q9 = QuestionText("q9",
                             QuestionType::rant,
                             "Est-ce que tu as quelque chose sur le cœur ?",
                             pair(make_shared<QuestionNumber>(q10), AnimationTriggers::to_thinking));

const auto q8 = QuestionText("q8",
                             QuestionType::mealPlus,
                             "Quelque chose à ajouter à propos de tes repas ?",
                             pair(make_shared<QuestionText>(q9), AnimationTriggers::to_thinking));

const auto q7 = QuestionNumber("q7",
                               QuestionType::mealQuality,
                               "Sur une échelle croissante de qualité de 1 à 5, tu dirais que ton repas était plutôt...",
                               {
                                   {1, pair(make_shared<QuestionText>(q8), AnimationTriggers::to_sad)},
                                   {2, pair(make_shared<QuestionText>(q8), AnimationTriggers::to_sad)},
                                   {3, pair(make_shared<QuestionText>(q8), AnimationTriggers::to_thinking)},
                                   {4, pair(make_shared<QuestionText>(q8), AnimationTriggers::to_happy)},
                                   {5, pair(make_shared<QuestionText>(q8), AnimationTriggers::to_happy)},
                               },
                               1,
                               5,
                               1);

const auto q6 = QuestionHour("q6",
                             QuestionType::mealHour,
                             "Quand est-ce que tu as mangé pour la dernière fois ?",
                             {{pair(6 * 3600, 8 * 3600), pair(std::make_shared<QuestionNumber>(q7),
                                                              AnimationTriggers::to_happy)},
                              {pair(0, 6 * 3600 - 1), pair(std::make_shared<QuestionNumber>(q7),
                                                           AnimationTriggers::to_happy)},
                              {pair(8 * 3600+1, 24 * 3600 ), pair(std::make_shared<QuestionNumber>(q7),
                                                           AnimationTriggers::to_happy)}},
                             2);

const auto q5 = QuestionText("q5",
                             QuestionType::rant,
                             "Est-ce que tu as pris le temps de te détendre avant de te coucher ?",
                             pair(make_shared<QuestionHour>(q6), AnimationTriggers::to_thinking));

const auto q4 = QuestionNumber("q4",
                               QuestionType::sleepQuality,
                               "Sur une échelle croissante de 1 à 5, tu dirais que la qualité de ton sommeil est plutôt...",
                               {
                                   {1, pair(make_shared<QuestionText>(q5), AnimationTriggers::to_sad)},
                                   {2, pair(make_shared<QuestionText>(q5), AnimationTriggers::to_sad)},
                                   {3, pair(make_shared<QuestionText>(q5), AnimationTriggers::to_thinking)},
                                   {4, pair(make_shared<QuestionText>(q5), AnimationTriggers::to_happy)},
                                   {5, pair(make_shared<QuestionText>(q5), AnimationTriggers::to_happy)},
                               },
                               1,
                               5,
                               1);

const auto q3 = QuestionHour("q3",
                             QuestionType::sleepEnd,
                             "Quand t'es-tu levé ?",
                             {{pair(6 * 3600, 8 * 3600), pair(std::make_shared<QuestionNumber>(q4),
                                                              AnimationTriggers::to_happy)},
                              {pair(0, 6 * 3600 - 1), pair(std::make_shared<QuestionNumber>(q4),
                                                           AnimationTriggers::to_sad)},
                              {pair(8 * 3600, 24 * 3600), pair(std::make_shared<QuestionNumber>(q4),
                                                           AnimationTriggers::to_sad)}},
                             2);

const auto q2 = QuestionHour("q2",
                             QuestionType::sleepBegin,
                             "Quand t'es-tu couché ?",
                             {{pair(22 * 3600, 24 * 3600), pair(std::make_shared<QuestionHour>(q3),
                                                                AnimationTriggers::to_sad)},
                              {pair(0, 22 * 3600 - 1), pair(std::make_shared<QuestionHour>(q3),
                                                            AnimationTriggers::to_happy)}},
                             2);

const auto q1 = QuestionAdjective("q1",
                                  QuestionType::adjective,
                                  "Hey ! Comment est-ce que tu te sens en ce moment ?",
                                  {
                                      {"content", pair(make_shared<QuestionHour>(q2),
                                                       AnimationTriggers::to_happy)},
                                      {"confiant", pair(make_shared<QuestionHour>(q2),
                                                       AnimationTriggers::to_happy)},
                                      {"triste", pair(make_shared<QuestionHour>(q2),
                                                      AnimationTriggers::to_happy)},
                                      {"en colère", pair(make_shared<QuestionHour>(q2),
                                                       AnimationTriggers::to_happy)},
                                      {"paniqué", pair(make_shared<QuestionHour>(q2),
                                                      AnimationTriggers::to_happy)},
                                      {"fatigué", pair(make_shared<QuestionHour>(q2),
                                                            AnimationTriggers::to_happy)},

                                  });
#endif // HATCHCARE_QUESTIONDATAS_H
