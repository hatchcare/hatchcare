<!--suppress HtmlUnknownAnchorTarget, CheckImageSize -->
<h1 style="text-align: center;">
  <br>
  Bienvenue sur HatchCare !
  <br>
</h1>

<p align="center">
  <a href="#features">Features</a> | 
  <a href="#compilation">Compilation</a> | 
  <a href="#Documentation">Documentation</a> | 
  <a href="#diagrammes">Diagrammes</a>
</p>

![Banner](./assets/banner.png)


<h4 text-align="center">Bienvenue sur Hatchcare ! Cette application passe-partout vous aidera à garder trace de votre santé mentale dynamiquement, au quotidien. Grâce à HatchCare, vous trouverez des pistes pour progresser dans votre manière de prendre soin de vous ; cela de manière individualisée grâce à vos réponses ! </h4>

# Features

<p align="center">
  <img src="./assets/screenshots/accueil.jpg" width="30%" alt="Accueil Screenshot">
  <img src="assets/screenshots/graph.jpg" width="30%" alt="Graph Screenshot">
  <img src="assets/screenshots/analysis.jpg" width="30%" alt="Analysis Screenshot">
</p>

# Architecture globale

Cette application est composée de deux parties : une partie "backend", codée en C++, et une partie front-end, codée en Dart.

## C++

Elle se trouve dans le dossier `libs/`.

La partie C++ de l'application est composée de plusieurs classes, qui permettent de gérer les données de l'utilisateur, de les analyser, et de les comparer. Ces classes sont ensuite utilisées par la partie Dart de l'application pour afficher les résultats.

## Dart

Elle se trouve dans le dossier `app/`.

La partie Dart de l'application est composée de plusieurs pages, qui permettent à l'utilisateur de rentrer ses données, afin de les passer à la partie C++ pour l'analyse. Les résultats de cette analyse sont ensuite affichés à l'utilisateur, sous forme de graphiques et de statistiques de corrélation.

## Communication

La partie C++ est "wrappée" en C, puis en Dart/Flutter, pour permettre la communication entre les deux parties de l'application.
En effet, Dart ne supporte par l'interfaçage direct avec du C++, il a donc fallu passer par une étape intermédiaire.

Le wrapper C est écrit à la main et consiste à utiliser les pointeurs vers void pour passer des données entre le C++ et le Dart.

Afin d'éviter les problèmes de mémoire, les pointeurs sont encapsulés dans des struct afin de toujours savoir quel type de classe C++ se trouve derrière le pointeur.

On dispose également de fonction de libération de mémoire pour chaque type de struct.

L'interfaçage entre le C et le Dart est fait via ffigen, qui permet de générer des bindings Dart à partir de code C.
```bash
dart run ffigen --config config.yaml
```

# Compilation

## nix

Si vous voulez obtenir toutes les dépendances nécessaires pour compiler le projet, vous pouvez utiliser le fichier `flake.nix` pour obtenir un environnement Nix complet.
Pour cela, il vous suffit d'avoir Nix avec Flake d'activer et de lancer : `nix develop` dans le répertoire du projet.

## C++

### Prérequis (si vous n'utilisez pas nix)

- CMake
- Clang

### Compilation

```bash
cd libs/
cmake -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
cmake --build build
```

### lancer

```bash
./build/hatchcare
```

## Dart/Flutter

### Prérequis (si vous n'utilisez pas nix)

- Flutter (en suivant les instructions sur le site de Flutter pour votre OS pour des applications desktop + mobile)
  - Pour Windows après avoir suivi les instructions d'installation de Flutter sur Windows, ne pas oublier d'installer llvm et Clang via
    Visual Studio

### Compilation

```bash
flutter build {windows,linux,apk}
```

En cas de problème, ne pas hésiter à lancer `flutter clean`

### lancer

```bash
# pour linux
./build/linux/release/bundle/hatchcare
# pour windows
./build/windows/runner/Release/hatchcare.exe
# pour android
adb install build/app/outputs/flutter-apk/app-release.apk
```

## Diagramme de classes

### cpp

```bash
cd libs/
cmake -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
clang-uml --progress
plantuml -tsvg -nometadata ../diagrams/*.puml
```

### flutter

```bash
dart pub global run dcdg > diagrams/hatchcare_dart.puml
plantuml -tsvg -nometadata diagrams/hatchcare_dart.puml
```

## Documentation

### C++

```bash
cd libs/
doxygen doc/doxyfile
```

Vous pouvez maintenant consulter la documentation dans `libs/doc/html/index.html`

### Dart

L'application n'étant pas un package, la documentation de dartdoc est très succinte.
```bash
dart doc
```
(vous aurez peut-être besoin de mettre une valeur dans la variable d'environnement FLUTTER_ROOT)

Vous pouvez maintenant consulter la documentation dans `doc/api/index.html`

# Diagrammes

[Diagramme cpp](./diagrams/hatchcare_cpp.svg)

[Diagramme dart](./diagrams/hatchcare_dart.svg)

Le diagramme de Gantt se trouve dans libs/doc/Diagramme_Gantt.pdf.

# Présentation du projet

La présentation PDF du projet se trouve dans libs/doc/Présentation_HatchCare.pdf

