# la big TODO

- [x] rendre l'appli belle !
- [ ] commenter tout le code dans toute l'appli
- [ ] faire fonctionner l'app sur macos
- [ ] chercker fonctionnement sur windows à la fin
- [ ] éviter les fautes d'orthographes
- [x] build on android
- [x] pouvoir mettre des range de date pour les question suivant d'heure
- [ ] si on a le temps, ajouter analyse de régularité de sommeil mdr + durrée de sommeil
- [ ] faire des questionnn
- [x] regarder si on peu faire un beau typedef pour tout les type dedes question plutot que répeter le variant tout le
  temps
- [x] clean en mettans des using namespace std et supprimer les std::
- [x] clean les unsused imports
- [x] const partouuut
- [x] create free method for every type of wrapper
- [x] implement question hour in flutter
- [x] pouvoir mettre une grande catégorie pour les question d'adjectif pour choisir la question suivant
- [x] avoir aucun warning lors de la compilation et de l'analyser clion
    + [x] in reponsesList.cpp find a method to avoid including a cpp
- [x] rename tout en anglais ou francais
- [ ] affichage des donnée de texte en brute
- [ ] enlever le plot des adjectif c'est completement con ptdrr

## Readme

- [x] faire un diagramme de classe du cpp
- [ ] faire un diagramme de classe du dart
- [x] faire un diagramme de "classe" pour le wrapper C
- [ ] ecrire une présentation de l'app avec des future screenshot (on peut piquer le readme de onyx qui es plutot beau)
- [ ] expliquer comment le tout s'articule/faire un diagramme total
- [ ] expliquer comment compiler /installer linux, windows, mac

## Analyse

- [x] définir l'archi pour le calcul des corrélation entre les différentes données
- [x] ecrire les algo pour les calculs de corrélation
- [x] écrire le wrapper C pour les analyse

- [x] deplacer la fonction findIntCorrelation dans le correlation utility
- [ ] écrire des tests pour les calculs de corrélation
- [x] faire interface flutter pour les corrélations
    - work in progress

