{pkgs ? import <nixpkgs> {}}:
# Nix derivation for basic C++ project using clang
with pkgs;
  clangStdenv.mkDerivation {
    name = "hatchcare";
    src = ./.;

    buildInputs = [
      clang
      cmake
    ];

    installPhase = ''
      mkdir -p $out/bin
      mv hatchcare $out/bin/hatchcare
    '';
  }
