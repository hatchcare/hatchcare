{
  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-parts,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"];

      perSystem = {
        lib,
        system,
        config,
        ...
      }: let
        pkgs = import nixpkgs {
          inherit system;
        };
      in {
        packages.default = config.packages.HatchCare;
        packages.HatchCare = pkgs.callPackage ./default.nix {};
        devShells.default = let
          pkgs = import nixpkgs {
            inherit system;
            config = {
              allowUnfree = true;
              android_sdk.accept_license = true;
            };
          };
          buildToolsVersionForAapt2 = "34.0.0-rc4";
          androidComposition = pkgs.androidenv.composeAndroidPackages {
            # Installing both version for aapt2 and version that flutter wants
            buildToolsVersions = [buildToolsVersionForAapt2 "30.0.3"];
            platformVersions = ["34" "33" "31" "30" "29"];
            abiVersions = ["armeabi-v7a" "arm64-v8a" "x86" "x86_64"];
            includeEmulator = true;
            emulatorVersion = "34.1.9";
            toolsVersion = "26.1.1";
            platformToolsVersion = "33.0.3";
            includeSources = false;
            includeSystemImages = false;
            systemImageTypes = ["google_apis_playstore"];
            cmakeVersions = [ "3.22.1" "3.18.1" ];
            includeNDK = true;
            ndkVersions = [ "23.1.7779620" "25.1.8937393" ];
            useGoogleAPIs = false;
            useGoogleTVAddOns = false;
            extraLicenses = [
              "android-googletv-license"
              "android-sdk-arm-dbt-license"
              "android-sdk-license"
              "android-sdk-preview-license"
              "google-gdk-license"
              "intel-android-extra-license"
              "intel-android-sysimage-license"
              "mips-android-sysimage-license"
            ];
          };
          androidSdk = androidComposition.androidsdk;
          PWD = builtins.getEnv "PWD";
        in
          pkgs.mkShell {
            CHROME_EXECUTABLE = lib.getExe pkgs.chromium;
            ANDROID_SDK_ROOT = "${androidSdk}/libexec/android-sdk";
            ANDROID_NDK_ROOT = "${androidSdk}/libexec/android-sdk/ndk-bundle";
            ANDROID_AVD_HOME = "${PWD}/.android/avd";
            ANDROID_HOME = "${androidSdk}/libexec/android-sdk";
            FLUTTER_SDK = "${pkgs.flutter}";
            GRADLE_OPTS = "-Dorg.gradle.project.android.aapt2FromMavenOverride=${androidSdk}/libexec/android-sdk/build-tools/${buildToolsVersionForAapt2}/aapt2";
            LD_LIBRARY_PATH = "/home/eymeric/code_bidouille/projet/hatchcare/build/linux/x64/debug/bundle/lib/:/home/eymeric/code_bidouille/projet/hatchcare/build/linux/x64/release/bundle/lib/:/home/eymeric/code_bidouille/projet/hatchcare/build/linux/x64/profile/bundle/lib/";
               TEST=PWD;
            buildInputs = with pkgs; [
              #common
              gitlab-runner
              #pkgs.clang-tools
              #llvm.clang # clangd
              clang
              #clang-tools
              cmake
              ninja
              gcc
              valgrind
              doxygen
              graphviz
              llvm

              #flutter
              chromium
              flutter
              jdk17
              androidSdk
              cmake
              apksigner
              ninja
              gtk3
              gitlab-runner
              android-tools
              pkg-config
              pcre2
              mount
              util-linux.dev
              libselinux
              libsepol
              libthai
              libdatrie
              xorg.libXdmcp
              libxkbcommon
              libepoxy
              xorg.libXtst
            ];
          };
        formatter = pkgs.alejandra;
      };
    };
}
