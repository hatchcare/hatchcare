```mermaid
gantt
    title programme hatchcare
    dateFormat  YYYY-MM-DD
    section semaine 1
    Interface en flutter sans connection avec cpp           :a1, 2024-03-25, 7d
    analyse de données numérique     :2024-03-25, 7d
    Animation hatchip   :2024-03-25, 7d
    section semaine 2
    Wrapper C pour le projet cpp           :a2, after a1, 7d
    analyse de données non numérique     :after a1, 7d
    Interface en flutter sans connection avec cpp    :after a1, 7d
    section semaine 3
    Connection de flutter avec le c/cpp           :a3, after a2, 7d
    update du wrapper c     :after a2, 7d
    gestion de projet, presentation, diagramme class etc   :after a2, 7d    
```